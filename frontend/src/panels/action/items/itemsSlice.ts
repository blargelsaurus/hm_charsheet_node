import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface itemState {
	activePanel?: string;
}

const initialState: itemState = {
	activePanel: '',
};

export const itemSlice = createSlice({
	name: 'items',
	initialState,
	reducers: {
		changePanel: (state, action: PayloadAction<string>) => {
			state.activePanel = action.payload;
		},
	},
});

export const { changePanel } = itemSlice.actions;

export const selectPanel = (state: RootState) => state.items.activePanel;

export default itemSlice.reducer;
