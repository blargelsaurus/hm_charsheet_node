import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './items.module.css';

import { Icon } from '../../../globals/icon/Icon';
import { Button } from '../../../globals/button/Button';

import {} from './itemsSlice';
import { selectActive } from '../bar/actionbarSlice';

export function Items() {
	const isOpen = useSelector(selectActive);

	return (
		<>
			<div
				id="itemGroup"
				className={`${styles.itemsContainer} ${isOpen ? '' : 'hiddenBar'}`}
			></div>
		</>
	);
}
