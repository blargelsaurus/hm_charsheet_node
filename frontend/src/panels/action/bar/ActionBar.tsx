import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './actionBar.module.css';

import { Icon } from '../../../globals/icon/Icon';
import { Button } from '../../../globals/button/Button';

import { selectActive } from './actionbarSlice';

export function ActionBar() {
	const isActive = useSelector(selectActive);

	return (
		<>
			<div id="actionBar" className={styles.actionContainer}>
				<Icon name="die" size="medium" state={'title'} />
				<h3>Action Panel</h3>
				<Button
					type="toggleAttacks"
					ico="fist-raised"
					icoSize="medium"
					btnState="normal"
				/>
				<Button
					type="toggleSpells"
					ico="meteor"
					icoSize="medium"
					btnState="normal"
				/>
				<Button
					type="toggleSkills"
					ico="dice"
					icoSize="medium"
					btnState="normal"
				/>
				<Button
					type="toggleActionPanel"
					ico={isActive === true ? 'close' : 'open'}
					icoSize="medium"
					btnState="noraml"
				/>
			</div>
		</>
	);
}
