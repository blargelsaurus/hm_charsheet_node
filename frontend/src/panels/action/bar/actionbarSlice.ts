import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface actionbarState {
	active: boolean;
}

const initialState: actionbarState = {
	active: true,
};

export const actionbarSlice = createSlice({
	name: 'actionBar',
	initialState,
	reducers: {
		changeActive: (state) => {
			state.active = state.active === true ? false : true;
		},
	},
});

export const { changeActive } = actionbarSlice.actions;

export const selectActive = (state: RootState) => state.actionBar.active;

export default actionbarSlice.reducer;
