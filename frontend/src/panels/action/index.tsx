import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import '../../App.css';
import styles from './actionPanel.module.css';

import { ActionBar } from './bar/ActionBar';
import { Items } from './items/Items';

export function ActionPanel() {
	return (
		<div
			data-label="actionPanel"
			id="actionPanel"
			className={styles.panelContainer}
		>
			<div className={styles.panelContent}>
				<Items />
				<ActionBar />
			</div>
		</div>
	);
}
