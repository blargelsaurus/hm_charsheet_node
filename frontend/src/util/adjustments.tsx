const weaponDataAdjust = (data: any) => {
	return Object.keys(data).map((d) => {
		return {
			name: d,
			value: data[d],
			span:
				d === 'notes' || d === 'name' || d === 'ammoName' || d === 'shield'
					? 3
					: 1,
		};
	});
};

const itemDataAdjust = (data: any) => {
	return Object.keys(data).map((d) => {
		return {
			id: data['itemId'],
			name: d,
			value: data[d],
			span:
				d === 'notes' || d === 'name' || d === 'ammoName' || d === 'shield'
					? 3
					: 1,
		};
	});
};

const spellDataAdjust = (data: any) => {
	return data.map((d: any) => {
		return {
			name: d,
			value: d,
			span:
				d === 'notes' || d === 'name' || d === 'ammoName' || d === 'shield'
					? 3
					: 1,
		};
	});
};

export const adjustments = {
	weaponDataAdjust,
	itemDataAdjust,
	spellDataAdjust,
};
