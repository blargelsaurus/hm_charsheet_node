import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button } from '../button/Button';
import { Input } from '../inputs/Input';
import { addItem } from '../tables/general/generalSlice';

import { v4 as uuidv4 } from 'uuid';

export function AddItemForm({ containerID }: any) {
	const [name, setName] = useState<string>('');
	const [weight, setWeight] = useState<number>(0);
	const [container, setContainer] = useState(containerID);
	const dispatch = useDispatch();

	const clickEvent = () => {
		dispatch(
			addItem({
				itemId: uuidv4(),
				location: container,
				name: name,
				weight: weight,
			})
		);
		setName('');
		setWeight(0);
	};

	const handleChange = (val: any, type: string) => {
		switch (type) {
			case 'name':
				setName(val.toString());
				break;
			case 'weight':
				setWeight(parseInt(val));
				break;
		}
	};

	return (
		<>
			<div data-label="addItemForm">
				<Input
					type={'text'}
					valueType={'name'}
					label={'Name'}
					value={name}
					onChange={handleChange}
				/>
				<Input
					type={'number'}
					valueType={'weight'}
					label={'Weight (lbs.)'}
					value={weight}
					onChange={handleChange}
				/>
				<Button
					onClick={clickEvent}
					type={'addItem'}
					ico={'text'}
					btnText={'Add Item'}
				/>
			</div>
		</>
	);
}
