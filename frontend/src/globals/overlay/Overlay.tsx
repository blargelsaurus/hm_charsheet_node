import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { string } from 'yargs';
import styles from './overlay.module.css';

import { changeModalVis } from '../modal/modalSlice';

export interface OverlayProps {
	active: boolean;
}

export function Overlay({ active }: OverlayProps) {
	const dispatch = useDispatch();
	const overClass = active === true ? '' : 'inactive';

	const handleClick = (e: any) => {
		dispatch(changeModalVis(false));
	};

	return (
		<>
			<div
				id="overlay"
				className={`${styles.overlay} ${overClass}`}
				onClick={(e) => handleClick(e)}
			></div>
		</>
	);
}
