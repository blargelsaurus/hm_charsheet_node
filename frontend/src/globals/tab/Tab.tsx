import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './tab.module.css';
import '../../App.css';

import { changeActive, selectActive } from '../../tabs/tabNav/tabnavSlice';

export interface TabProps {
	type: string;
	name: string;
	key: string;
}

export function Tab({ type, name }: TabProps) {
	const dispatch = useDispatch();

	const currentPrimaryTab = useSelector(selectActive);

	const checkPrimaryTab = (name: string) => {
		return currentPrimaryTab === name ? true : false;
	};

	const handleClick = (e: any) => {
		switch (type) {
			case 'primary':
				dispatch(changeActive(name));
				break;

			default:
				break;
		}
	};

	const isActive =
		type === 'primary' ? (checkPrimaryTab(name) === true ? 'active' : '') : '';

	return (
		<>
			<div
				data-label={name}
				data-type={`${type}tab`}
				className={`${styles.tab} ${isActive}`}
			>
				<button onClick={(e) => handleClick(e)}>{name}</button>
			</div>
		</>
	);
}
