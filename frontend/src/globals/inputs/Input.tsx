import styles from './input.module.css';

interface inputProp {
	label: string;
	type: string;
	onChange: any;
	valueType: string;
	value?: any;
}

export function Input({ label, type, onChange, valueType, value }: inputProp) {
	const handleChange = (e: any) => {
		onChange(e.target.value, valueType);
	};

	return (
		<>
			<div className={styles.inputContainer}>
				<label>{label}</label>
				<input
					data-label={`${label}-input`}
					type={type}
					onChange={(e) => handleChange(e)}
					value={value}
				/>
			</div>
		</>
	);
}
