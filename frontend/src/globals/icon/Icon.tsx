import styles from './icon.module.css';
import '../../App.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faAngleLeft,
	faAngleRight,
	faDiceD6,
	faMeteor,
	faFistRaised,
	faDice,
	faPlusSquare,
	faMinusSquare,
} from '@fortawesome/free-solid-svg-icons';

export interface IconProps {
	name: string;
	size?: string;
	state?: string;
}

export function Icon({ name, state }: IconProps) {
	return (
		<>
			<div
				data-label={`${name}_icon`}
				className={`${styles.icon} btn_${state}`}
			>
				{renderSwitch(name)}
			</div>
		</>
	);

	function renderSwitch(params: string) {
		switch (params) {
			case 'fist-raised':
				return <FontAwesomeIcon icon={faFistRaised} />;

			case 'die':
				return <FontAwesomeIcon icon={faDiceD6} />;

			case 'meteor':
				return <FontAwesomeIcon icon={faMeteor} />;

			case 'dice':
				return <FontAwesomeIcon icon={faDice} />;

			case 'close':
				return <FontAwesomeIcon icon={faAngleLeft} />;

			case 'open':
				return <FontAwesomeIcon icon={faAngleRight} />;

			case 'expand':
				return <FontAwesomeIcon icon={faPlusSquare} />;

			case 'collapse':
				return <FontAwesomeIcon icon={faMinusSquare} />;

			default:
				break;
		}
	}
}
