import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './button.module.css';

import { Icon } from '../icon/Icon';
import {
	changeActive,
	selectActive,
} from '../../panels/action/bar/actionbarSlice';

import { changePanel, selectPanel } from '../../panels/action/items/itemsSlice';
import { changeModalVis } from '../modal/modalSlice';
import { toggleSpellInfo } from '../tables/spells/spellsSlice';

export interface ButtonProps {
	type: string;
	ico: string;
	icoSize?: string;
	btnState?: string;
	btnText?: string;
	isAlone?: boolean;
	onClick?: any;
	data?: any;
	customClass?: string;
}

export function Button({
	type,
	ico,
	icoSize,
	btnState,
	btnText,
	isAlone,
	onClick,
	data,
	customClass,
}: ButtonProps) {
	const currPanel = useSelector(selectPanel);
	const isActive = useSelector(selectActive);
	const dispatch = useDispatch();
	const [ogState, setOgState] = useState(btnState);
	const [currState, setCurrState] = useState(btnState);
	const [hover, setHover] = useState(false);

	const handleClick = (e: any, state: string, btnText?: string) => {
		if (btnText) {
			switch (btnText.toLowerCase()) {
				case 'close':
					dispatch(changeModalVis(false));
					break;
				case 'add item':
					onClick(data);
					break;
				case 'togglespellinfo':
					dispatch(toggleSpellInfo(data.spell));
					break;
				default:
					break;
			}
		} else {
			isActive === false && dispatch(changeActive());
			type !== 'toggleActionPanel' ? makeActive() : togglePanel();
		}
	};

	const makeActive = () => {
		setCurrState('active');
		setOgState('active');

		switch (type) {
			case 'toggleAttacks':
			case 'toggleSpells':
			case 'toggleSkills':
				dispatch(changePanel(type));
				break;

			default:
				break;
		}
	};

	const togglePanel = () => {
		dispatch(changeActive());
		currPanel !== '' && dispatch(changePanel(''));
	};

	const handleHover = (e: any) => {
		hover === false ? setHover(true) : setHover(false);
	};

	const returnSwitch = (ico: string, btnText?: string): any => {
		switch (ico) {
			case 'text':
				return <label>{btnText}</label>;

			default:
				return <Icon name={ico} size={icoSize} state={currState} />;
		}
	};

	useEffect(() => {
		currPanel !== type && setCurrState('normal');
	}, [currPanel]);

	useEffect(() => {
		hover === true ? setCurrState('hover') : setCurrState(ogState);
	}, [hover]);

	useEffect(() => {
		setCurrState(ogState);
	}, []);

	return (
		<>
			<button
				data-label={`${type}_btn`}
				className={`${styles.button} ${btnText ? 'hasText' : ''} ${
					isAlone ? 'isAlone' : ''
				} ${customClass ? customClass : ''}`}
				onClick={(e) => handleClick(e, 'active', btnText)}
				onMouseOver={(e) => handleHover(e)}
				onMouseLeave={(e) => handleHover(e)}
			>
				{returnSwitch(ico, btnText)}
			</button>
		</>
	);
}
