import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './tooltip.module.css';

export interface TooltipProps {
	text: string;
}

export function Tooltip({ text }: TooltipProps) {
	return (
		<>
			<div className={styles.tooltip}>
				<p>{text}</p>
			</div>
		</>
	);
}
