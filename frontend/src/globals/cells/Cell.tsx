import React from 'react';
import { useDispatch } from 'react-redux';
import styles from './cell.module.css';

import { removeItem } from '../tables/general/generalSlice';

interface cellProps {
	data: any;
}

export function Cell({ data }: cellProps) {
	const dispatch = useDispatch();
	const label: string = data?.name || '';
	const val: number = data?.value || 0;
	const span: number = data?.span || 1;

	const handleClick = (e: React.MouseEvent, tag: string, data: any) => {
		e.preventDefault();

		switch (tag) {
			case 'removeItem':
				dispatch(removeItem(data.id));
				break;

			default:
				break;
		}
	};

	const getContent = (type: string, val: string | number, data: any) => {
		switch (type) {
			case 'btns':
				return (
					<p className={styles.removeItem}>
						<a
							href={''}
							className={styles.removeBtn}
							onClick={(e) => handleClick(e, 'removeItem', data)}
							data-label="removeItem"
						>
							remove
						</a>
					</p>
				);

			default:
				return <p>{val}</p>;
		}
	};

	return (
		<>
			<div
				className={`${styles.tableCell} cell colspan-${span}`}
				data-label={label}
			>
				{getContent(label, val, data)}
			</div>
		</>
	);
}
