import styles from './column.module.css';

import { v4 as uuidv4 } from 'uuid';

interface columnProps {
	data: any;
	len: number;
	grids: number;
	spanLen: number;
}

export function Column({ data, spanLen }: columnProps) {
	const colLabel = data?.name || '';
	const colName = data?.abbrev || '';
	const subs = data?.subColumns || [];
	const subLen = subs.length || 0;
	// const override = spanLen;

	const getSubs = (subs: any) => {
		return (
			<ul className={`grid-col-${subs.length} subTable`}>
				{subs.map((col: any, idx: number, subs: any) => {
					return (
						<li key={uuidv4()} className={`col-1-${subs.length}`}>
							{col}
						</li>
					);
				})}
			</ul>
		);
	};

	return (
		<>
			<div
				className={`${styles.tableCell} header colspan-${
					subLen ? subLen : spanLen > 1 ? spanLen : '1'
				} ${subLen ? 'hasSubs' : ''}`}
				data-label={colLabel}
			>
				<p>{colName}</p>
				{getSubs(subs)}
			</div>
		</>
	);
}
