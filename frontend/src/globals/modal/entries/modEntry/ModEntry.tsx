import { useDispatch } from 'react-redux';
import styles from './modEntry.module.css';

import { v4 as uuidv4 } from 'uuid';

import { addContainer } from '../../../tables/general/generalSlice';

export interface modProps {
	data: any;
	type: string;
}

export function ModEntry({ data, type }: modProps) {
	const dispatch = useDispatch();

	const handleClick = (e: any, data: any, type: string) => {
		switch (type) {
			case 'addContainer':
				const containerData = { ...data };
				containerData['id'] = uuidv4();
				dispatch(addContainer(containerData));
				break;

			default:
				break;
		}
	};

	const getContent = (data: any, type: string) => {
		switch (type) {
			case 'statmods':
				return (
					<>
						<div className={styles.item}>
							<h3>{data?.name || ''}</h3>
							<p>{data?.desc || ''}</p>
						</div>
						<div className={styles.item}>
							<h3>{data?.stat || ''}</h3>
						</div>
						<div className={styles.item}>
							<h3>{`${data?.increase ? '' : '-'}${data?.change || 0}`}</h3>
						</div>
					</>
				);

			case 'addContainer':
				return data.map((d: any) => {
					return (
						<button data-key={d?.key} onClick={(e) => handleClick(e, d, type)}>
							<div className={styles.item}>
								<h3>{d?.name || ''}</h3>
								<p>Type</p>
							</div>
							<div className={styles.item}>
								<h3>{d?.weightMax || ''}</h3>
								<p>Wgt. (lbs)</p>
							</div>
						</button>
					);
				});

			default:
				return <></>;
		}
	};

	return (
		<>
			<div data-label={type} className={styles.modEntryContainer}>
				{getContent(data, type)}
			</div>
		</>
	);
}
