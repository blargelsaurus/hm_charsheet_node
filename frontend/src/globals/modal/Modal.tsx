import { useSelector } from 'react-redux';
import styles from './modal.module.css';
import { v4 as uuidv4 } from 'uuid';

import { modalState } from './modalSlice';
import { Button } from '../button/Button';
import { selectModalData } from './modalSlice';

import { ModEntry } from './entries/modEntry/ModEntry';
import { AddItemForm } from '../forms/AddItem';

export function Modal({ active }: modalState) {
	const modalClass = active === true ? '' : 'inactive';
	const data = useSelector(selectModalData);
	const title = data?.type || '';
	const color = data?.color || 'darkgrey';
	const parent = data?.parentId || '';

	const getTitle = (type: string) => {
		let title = '';
		switch (type) {
			case 'statmods':
				title = 'Bonuses & Penalties';
				break;

			case 'addContainer':
				title = 'Add New Container';
				break;

			case 'addItem':
				title = 'Add Item';
				break;

			default:
				title = 'Title missing...';
				break;
		}

		return <h3>{title}</h3>;
	};

	const getBodyContent = (data: any, type: string) => {
		let response;

		if (data !== undefined) {
			let keys = Object.keys(data);
			response = keys.map((k) => {
				switch (type) {
					case 'statmods':
					case 'addContainer':
						return <ModEntry key={uuidv4()} data={data[k]} type={type} />;

					default:
						return '';
				}
			});
		} else {
			switch (type) {
				case 'addItem':
					return <AddItemForm containerID={parent} />;

				default:
					return <></>;
			}
		}

		return response;
	};

	return (
		<>
			<div id="modal" className={`${styles.modalContainer} ${modalClass}`}>
				<div className={`${styles.title} ${color}`}>{getTitle(title)}</div>
				<div className={styles.body}>
					<div className={styles.bodyContent}>
						{getBodyContent(data.data, title)}
					</div>
					<Button
						type="toggleModal"
						ico="text"
						icoSize="medium"
						btnState="normal"
						btnText="Close"
						isAlone={true}
					/>
				</div>
			</div>
		</>
	);
}
