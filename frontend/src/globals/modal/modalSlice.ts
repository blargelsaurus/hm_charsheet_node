import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface modalState {
	data?: any;
	type?: string;
	active: boolean;
	color?: string;
	parentId?: string | number;
}

const initialState: modalState = {
	data: {},
	type: '',
	active: false,
	color: '',
	parentId: '',
};

export const modalSlice = createSlice({
	name: 'modal',
	initialState,
	reducers: {
		changeModalVis: (state, action: PayloadAction<boolean>) => {
			state.active = action.payload;
		},
		openModal: (state, action: PayloadAction<any>) => {
			state.active = action.payload.active;
			state.data = action.payload.data;
			state.type = action.payload.type;
			state.color = action.payload.color;
			state.parentId = action.payload?.parentid || '';
		},
	},
});

export const { changeModalVis, openModal } = modalSlice.actions;

export const isModalActive = (state: RootState) => state.modal.active;
export const selectModalData = (state: RootState) => state.modal;

export default modalSlice.reducer;
