import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './row.module.css';

import { v4 as uuidv4 } from 'uuid';
import { Cell } from '../cells/Cell';

import { adjustments } from '../../util/adjustments';
import { Button } from '../button/Button';
import { Spell } from '../tables/spells/Spell';

interface rowProps {
	data: any;
	type: string;
	extra?: any;
	isOdd?: boolean;
}

export function Row({ data, type, extra, isOdd }: rowProps) {
	const ignore = ['location', 'itemId'];

	const getCells = (data: any) => {
		return Object.keys(data).map((k) => {
			if (ignore.indexOf(data[k]?.name) === -1)
				return <Cell key={uuidv4()} data={data[k]} />;
		});
	};

	switch (type.toLowerCase()) {
		case 'weapon':
		case 'ammo':
		case 'armor':
		case 'shield':
		case 'talents':
		case 'skills':
		case 'racial':
		case 'proficiency':
		case 'quirks':
		case 'languages':
			return <>{getCells(adjustments.weaponDataAdjust(data))}</>;

		case 'items':
			return <>{getCells(adjustments.itemDataAdjust(data))}</>;

		case 'spells':
			return <Spell data={data} extra={extra} isOdd={isOdd} />;

		default:
			return <></>;
	}
}
