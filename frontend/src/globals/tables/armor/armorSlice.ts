import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface armor {
	name: string;
	weight: number;
	ac: number;
	acNeg1: number | string;
	ac0: number | string;
	ac1: number | string;
	ac2: number | string;
	ac3: number | string;
	ac4: number | string;
	ac5: number | string;
	ac6: number | string;
	ac7: number | string;
	ac8: number | string;
	ac9: number | string;
	ac10: number | string;
	type: string;
}

export interface armorState {
	columns: {
		[key: string]: column;
	};
	armor: {
		[key: string]: armor;
	};
}

const initialState: armorState = {
	columns: {
		'0': {
			id: 0,
			name: 'Armor',
			abbrev: 'Armor',
		},
		'1': {
			id: 1,
			name: 'Weight',
			abbrev: 'Wgt.',
		},
		'2': {
			id: 2,
			name: 'AC',
			abbrev: 'AC',
		},
		'3': {
			id: 3,
			name: 'AC Points',
			abbrev: 'Pts.',
			subColumns: [
				'-1',
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'10',
			],
		},
		'4': {
			id: 4,
			name: 'Type',
			abbrev: 'Type',
		},
	},
	armor: {
		'0': {
			name: 'Plate Mail',
			weight: 12.5,
			ac: 3,
			acNeg1: '',
			ac0: '',
			ac1: '',
			ac2: '',
			ac3: 12,
			ac4: 10,
			ac5: 8,
			ac6: 6,
			ac7: 4,
			ac8: 2,
			ac9: 1,
			ac10: 0,
			type: 'Body',
		},
	},
};

export const armorSlice = createSlice({
	name: 'armor',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = armorSlice.actions;

export const getArmor = (state: RootState) => state.armor.armor;
export const getArmorCol = (state: RootState) => state.armor.columns;

export default armorSlice.reducer;
