import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './spell.module.css';

import { Button } from '../../button/Button';
import { Cell } from '../../cells/Cell';
import { SpellInfo } from './SpellInfo';

interface spellProps {
	data: any;
	extra: any;
	isOdd: boolean | undefined;
}

export function Spell({ data, extra, isOdd }: spellProps) {
	const spellSlots = `${data.memorized}/${extra?.spellSlots || ''}`;

	return (
		<div
			data-id={data.id}
			className={`${styles.spell} colspan-14 ${isOdd === false ? 'isOdd' : ''}`}
		>
			<div className={styles.primary}>
				<Button
					type={'spell'}
					btnText={'toggleSpellInfo'}
					ico={`${data.active === true ? 'collapse' : 'expand'}`}
					customClass={`colspan-1 hasCellBorder simpleBtn`}
					data={{ spell: data.name }}
				/>
				<Cell data={{ name: 'Name', value: data.name, span: 3 }} />
				<Cell data={{ name: 'Level', value: data.level, span: 1 }} />
				<Cell data={{ name: '# Memorized', value: spellSlots, span: 1 }} />
				<Cell data={{ name: 'Cast Time', value: data.castTime, span: 1 }} />
				<Cell data={{ name: 'Damage', value: data.damage, span: 1 }} />
				<Cell data={{ name: 'Range', value: data.range, span: 1 }} />
				<Cell data={{ name: 'Duration', value: data.duration, span: 1 }} />
				<Cell data={{ name: 'Components', value: data.components, span: 1 }} />
				<Cell data={{ name: 'Area of Effect', value: data.aoe, span: 3 }} />
			</div>
			<div
				className={`${styles.secondary} ${
					data.active === true ? 'isActive' : 'isHidden'
				}`}
			>
				<h4>Additional Information</h4>
				<div>
					<SpellInfo name={'Save'} value={data.extras.save} />
					<SpellInfo name={'School of Magic'} value={data.extras.school} />
					<SpellInfo
						name={'Pages in Spellbook'}
						value={data.extras.spellBookPgs}
					/>
					<SpellInfo name={'Source Book'} value={data.extras.book} />
					<SpellInfo name={'Found on Page'} value={data.extras.page} />
				</div>
			</div>
		</div>
	);
}
