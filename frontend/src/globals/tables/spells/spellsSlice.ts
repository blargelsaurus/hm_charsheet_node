import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface spellExtra {
	save: string;
	school: string;
	spellBookPgs: number;
	materials?: spellMaterials;
	book: string;
	page: number;
}

export interface spellMaterials {
	Material: string;
	Quantity: number;
}

export interface spell {
	id: string;
	active: boolean;
	name: string;
	level: number;
	memorized: number;
	castTime: string;
	damage?: string;
	range: string;
	duration: string;
	components: string;
	aoe: string;
	extras: spellExtra;
}

export interface spellState {
	columns: column[];
	spells: spell[];
	spellSlots: number;
	hasSpells: boolean;
}

const initialState: spellState = {
	columns: [
		{
			id: 0,
			name: 'Active',
			abbrev: 'Active',
		},
		{
			id: 1,
			name: 'Name',
			abbrev: 'Name',
		},
		{
			id: 2,
			name: 'Level',
			abbrev: 'Lvl.',
		},
		{
			id: 3,
			name: 'Memorized',
			abbrev: '# mem.',
		},
		{
			id: 4,
			name: 'Cast Time',
			abbrev: 'Time',
		},
		{
			id: 5,
			name: 'Damage',
			abbrev: 'Dmg.',
		},
		{
			id: 6,
			name: 'Range',
			abbrev: 'Range',
		},
		{
			id: 7,
			name: 'Duration',
			abbrev: 'Dur.',
		},
		{
			id: 8,
			name: 'Components',
			abbrev: 'Comp.',
		},
		{
			id: 9,
			name: 'Area of Effect',
			abbrev: 'AOE',
		},
	],
	spells: [
		{
			id: 'some-guid',
			active: false,
			name: 'Spook',
			level: 1,
			memorized: 2,
			castTime: '1 seg',
			damage: 'N/A',
			range: '30 feet',
			duration: 'Special',
			components: 'V,S',
			aoe: '1 Creature',
			extras: {
				save: 'Negates',
				school: 'Ill/Pha',
				spellBookPgs: 5,
				book: 'PHB',
				page: 185,
			},
		},
		{
			id: 'some-guid2',
			active: false,
			name: 'Write Magic',
			level: 1,
			memorized: 1,
			castTime: '1 round',
			damage: 'N/A',
			range: 'Special',
			duration: '1 Hour/Lvl',
			components: 'V,S,M',
			aoe: '1 Spell Inscription',
			extras: {
				save: 'Special',
				school: 'Evocation',
				spellBookPgs: 5,
				book: 'PHB',
				page: 186,
			},
		},
		{
			id: 'some-guid3',
			active: false,
			name: 'Read Magic',
			level: 1,
			memorized: 1,
			castTime: '1 Round',
			damage: 'N/A',
			range: 'Special',
			duration: '2 Round/Lvl',
			components: 'V,S,M',
			aoe: 'Self',
			extras: {
				save: 'N/A',
				school: 'Divination',
				spellBookPgs: 5,
				book: 'PHB',
				page: 184,
			},
		},
	],
	spellSlots: 7,
	hasSpells: true,
};

export const spellSlice = createSlice({
	name: 'spells',
	initialState,
	reducers: {
		changeActive: (state) => {},
		toggleSpellInfo: (state, action: PayloadAction<string>) => {
			const thisSpell = state.spells.filter((s) => s.name === action.payload);

			if (thisSpell) {
				const infoState = thisSpell[0].active;
				thisSpell[0].active = infoState === true ? false : true;
			}
		},
	},
});

export const { changeActive, toggleSpellInfo } = spellSlice.actions;

export const getSpells = (state: RootState) => state.spells.spells;
export const getSpellsCol = (state: RootState) => state.spells.columns;
export const getSpellsSlots = (state: RootState) => state.spells.spellSlots;
export const hasSpells = (state: RootState) => state.spells.hasSpells;

export default spellSlice.reducer;
