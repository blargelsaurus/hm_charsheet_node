import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './spell.module.css';

interface spellInfoProps {
	name: string;
	value: string | number;
}

export function SpellInfo({ name, value }: spellInfoProps) {
	return (
		<ul className={styles.infoItem}>
			<li>
				<strong>{name}</strong>
			</li>
			<li>{value}</li>
		</ul>
	);
}
