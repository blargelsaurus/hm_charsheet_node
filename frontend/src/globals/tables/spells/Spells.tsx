import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './spells.module.css';

import {
	column,
	getSpells,
	getSpellsCol,
	getSpellsSlots,
	hasSpells,
} from './spellsSlice';
import { Column } from '../../column/Column';

import { v4 as uuidv4 } from 'uuid';
import { Row } from '../../rows/Row';

export function Spells() {
	const columns = useSelector(getSpellsCol);
	const spells = useSelector(getSpells);
	const spellSlots = useSelector(getSpellsSlots);
	const isMU = useSelector(hasSpells);

	const [spl, setSpl] = useState(spells);

	useEffect(() => {
		setSpl(spells);
	}, [spells]);

	const returnColumns = (columns: any) => {
		let totalCells: number = 0;

		Object.keys(columns).forEach((k: string) => {
			totalCells += columns[k].subColumns
				? columns[k].subColumns.length
				: columns[k].name === 'Name' || columns[k].name === 'AOE'
				? 3
				: 1;
		});

		return Object.keys(columns).map((k: string, idx: number, arr: any) => (
			<Column
				key={uuidv4()}
				data={columns[k]}
				len={arr.length}
				grids={totalCells}
				spanLen={
					columns[k]?.name === 'Name' || columns[k]?.name === 'Area of Effect'
						? 3
						: 1
				}
			/>
		));
	};

	const returnRows = (spells: any) => {
		const extras = { spellSlots: spellSlots };

		return spells.map((k: string, idx: number, arr: any) => (
			<Row
				key={uuidv4()}
				data={k}
				type={'spells'}
				extra={extras}
				isOdd={idx % 2 === 0 ? false : true}
			/>
		));
	};

	return (
		<>
			{isMU && (
				<div id="spellContainer" className={styles.spellContainer}>
					<h3>Spells</h3>

					<div className={styles.table}>
						{returnColumns(columns)}
						{returnRows(spells)}
					</div>
				</div>
			)}
		</>
	);
}
