import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface racial {
	itemId?: number | string;
	name: string;
}

export interface racialState {
	columns: {
		[key: string]: column;
	};
	racials: racial[];
}

const initialState: racialState = {
	columns: {
		'0': {
			id: 0,
			name: 'Name',
			abbrev: 'Name',
		},
	},
	racials: [
		{
			itemId: 1,
			name: 'Detect grade/slope in passage 1-5 on 1d6',
		},
		{
			itemId: 2,
			name: 'Detect unsafe walls, ceilings, and floors 1-7 on 1d10',
		},
		{
			itemId: 3,
			name: 'Determine approximate depth underground 1-4 on 1d6',
		},
		{
			itemId: 4,
			name: 'Determine approximate direction underground 1-3 on 1d6',
		},
		{
			itemId: 3,
			name: 'Groin Stomp Attack (Special Combat Maneuver',
		},
	],
};

export const racialSlice = createSlice({
	name: 'racials',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = racialSlice.actions;

export const getRacials = (state: RootState) => state.racials.racials;
export const getRacialCol = (state: RootState) => state.racials.columns;

export default racialSlice.reducer;
