import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './racial.module.css';

import { getRacials, getRacialCol } from './racialSlice';
import { General } from '../general/General';

export function Racials() {
	const racials = useSelector(getRacials);
	const columns = useSelector(getRacialCol);
	// const [tal, setTal] = useState(talents);

	// useEffect(() => {
	// 	setTal(talents);
	// }, [talents]);

	return (
		<>
			<div id="racialContainer" className={styles.racialContainer}>
				<General
					label={'Racial'}
					columns={1}
					data={racials}
					colInfo={columns}
					loadAll={true}
				/>
			</div>
		</>
	);
}
