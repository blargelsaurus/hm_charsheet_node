import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './weapons.module.css';

import { column, getWeaponCol, getWeapons } from './weaponsSlice';
import { Column } from '../../column/Column';

import { v4 as uuidv4 } from 'uuid';
import { Row } from '../../rows/Row';

export function Weapons() {
	const columns = useSelector(getWeaponCol);
	const weapons = useSelector(getWeapons);

	const [weap, setWeap] = useState(weapons);

	useEffect(() => {
		setWeap(weapons);
	}, [weapons]);

	const returnColumns = (columns: any) => {
		let totalCells: number = 0;

		Object.keys(columns).forEach((k: string) => {
			totalCells += columns[k].subColumns
				? columns[k].subColumns.length
				: columns[k].name === 'Notes'
				? 3
				: 1;
		});

		return Object.keys(columns).map((k: string, idx: number, arr: any) => (
			<Column
				key={uuidv4()}
				data={columns[k]}
				len={arr.length}
				grids={totalCells}
				spanLen={
					columns[k]?.name === 'Notes' || columns[k]?.name === 'Weapon' ? 3 : 1
				}
			/>
		));
	};

	const returnRows = (weapons: any) => {
		return Object.keys(weapons).map((k: string, idx: number, arr: any) => (
			<Row key={uuidv4()} data={weapons[k]} type={'weapon'} />
		));
	};

	return (
		<>
			<div id="weaponsContainer" className={styles.weaponsContainer}>
				<h3>Weapons</h3>

				<div className={styles.table}>
					{returnColumns(columns)}
					{returnRows(weapons)}
				</div>
			</div>
		</>
	);
}
