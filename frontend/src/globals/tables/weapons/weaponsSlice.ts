import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	default?: string | number;
	subColumns?: string[];
}

export interface weapon {
	proficiency: number;
	name: string;
	magPlus: number;
	rof: number;
	size: string;
	speed: number;
	hitRanged: number;
	hitMelee: number;
	dmgRanged: number;
	dmgMelee: number;
	tinyDmg: string;
	smallDmg: string;
	medDmg: string;
	lgDmg: string;
	hugeDmg: string;
	giDmg: string;
	type: string;
	weight: number;
	notes: string;
	initiative: number;
}

export interface weaponsState {
	columns: {
		[key: string]: column;
	};
	weapons: {
		[key: string]: weapon;
	};
}

const initialState: weaponsState = {
	columns: {
		'0': {
			id: 0,
			name: 'Proficiency',
			abbrev: 'Prof.',
		},
		'1': {
			id: 1,
			name: 'Weapon',
			abbrev: 'Weapon',
		},
		'2': {
			id: 2,
			name: 'Magic Plus',
			abbrev: 'Mag. Plus',
			default: 0,
		},
		'3': {
			id: 3,
			name: 'Attack Speed/Rate of Fire',
			abbrev: '#Att./ ROF',
		},
		'4': {
			id: 4,
			name: 'Weapon Size',
			abbrev: 'Wpn. Size',
		},
		'5': {
			id: 5,
			name: 'Weapon Speed',
			abbrev: 'Wpn. Spd.',
		},
		'6': {
			id: 6,
			name: 'To-Hit Adjust',
			abbrev: 'To-Hit Adj.',
			subColumns: ['Ranged', 'Melee'],
		},
		'7': {
			id: 7,
			name: 'Damage Adjust',
			abbrev: 'Dmg. Adj.',
			subColumns: ['Ranged', 'Melee'],
		},
		'8': {
			id: 8,
			name: 'Damage Versus',
			abbrev: 'Damage vs.',
			subColumns: ['T /', 'S /', 'M /', 'L /', 'H /', 'G /'],
		},
		'9': {
			id: 9,
			name: 'Damage Type',
			abbrev: 'Type',
		},
		'10': {
			id: 10,
			name: 'Weight (Pounds)',
			abbrev: 'Wgt. (Lbs.)',
		},
		'11': {
			id: 11,
			name: 'Notes',
			abbrev: 'Notes',
		},
		'12': {
			id: 12,
			name: 'Initiate Bonus',
			abbrev: 'Init.',
		},
	},
	weapons: {
		'0': {
			proficiency: -1,
			name: 'Dagger - Far Reaching Vengeance',
			magPlus: 2,
			rof: 1,
			size: 'S',
			speed: -3,
			hitRanged: 9,
			hitMelee: 7,
			dmgRanged: 8,
			dmgMelee: 8,
			tinyDmg: '1d6+1',
			smallDmg: '1d6',
			medDmg: '1d6-1',
			lgDmg: '1d6-2',
			hugeDmg: '1d6-2',
			giDmg: '1d6-4',
			type: 'S',
			weight: 1,
			notes: 'Sample entry...',
			initiative: -3,
		},
		'1': {
			proficiency: 1,
			name: 'Short Sword',
			magPlus: 0,
			rof: 3 / 2,
			size: 'S',
			speed: -2,
			hitRanged: 0,
			hitMelee: 5,
			dmgRanged: 0,
			dmgMelee: 8,
			tinyDmg: '1d6-2',
			smallDmg: '1d6-1',
			medDmg: '1d6',
			lgDmg: '1d8',
			hugeDmg: '1d8',
			giDmg: '1d6',
			type: 'S',
			weight: 3,
			notes: 'Sample entry...',
			initiative: -2,
		},
		'2': {
			proficiency: -3,
			name: 'Fist',
			magPlus: 0,
			rof: 1,
			size: 'S',
			speed: -2,
			hitRanged: 0,
			hitMelee: 0,
			dmgRanged: 0,
			dmgMelee: 6,
			tinyDmg: '1d2',
			smallDmg: '1d2',
			medDmg: '1d2',
			lgDmg: '1d2',
			hugeDmg: '1d2',
			giDmg: '1d2',
			type: 'S',
			weight: 0,
			notes: 'Sample entry...',
			initiative: -2,
		},
	},
};

export const weaponsSlice = createSlice({
	name: 'weapons',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = weaponsSlice.actions;

export const getWeapons = (state: RootState) => state.weapons.weapons;
export const getWeaponCol = (state: RootState) => state.weapons.columns;

export default weaponsSlice.reducer;
