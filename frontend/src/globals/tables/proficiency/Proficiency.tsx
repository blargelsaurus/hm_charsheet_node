import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './proficiency.module.css';

import { getProfs, getProfCol } from './proficiencySlice';
import { General } from '../general/General';

export function Proficiency() {
	const profs = useSelector(getProfs);
	const columns = useSelector(getProfCol);
	// const [tal, setTal] = useState(talents);

	// useEffect(() => {
	// 	setTal(talents);
	// }, [talents]);

	return (
		<>
			<div id="profContainer" className={styles.profContainer}>
				<General
					label={'Proficiency'}
					columns={1}
					data={profs}
					colInfo={columns}
					loadAll={true}
				/>
			</div>
		</>
	);
}
