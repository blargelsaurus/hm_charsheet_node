import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface proficiency {
	itemId?: number | string;
	name: string;
}

export interface profState {
	columns: {
		[key: string]: column;
	};
	proficiencies: proficiency[];
}

const initialState: profState = {
	columns: {
		'0': {
			id: 0,
			name: 'Name',
			abbrev: 'Name',
		},
	},
	proficiencies: [
		{
			itemId: 1,
			name: 'Short Sword BP',
		},
		{
			itemId: 2,
			name: 'Short Sword Specialization',
		},
		{
			itemId: 3,
			name: 'Advanced Sword and Shield',
		},
	],
};

export const proficiencySlice = createSlice({
	name: 'proficiency',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = proficiencySlice.actions;

export const getProfs = (state: RootState) => state.proficiency.proficiencies;
export const getProfCol = (state: RootState) => state.proficiency.columns;

export default proficiencySlice.reducer;
