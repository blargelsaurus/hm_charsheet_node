import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './ammo.module.css';

import { column, getAmmo, getAmmoCol } from './ammoSlice';
import { Column } from '../../column/Column';

import { v4 as uuidv4 } from 'uuid';
import { Row } from '../../rows/Row';

export function Ammo() {
	const columns = useSelector(getAmmoCol);
	const ammo = useSelector(getAmmo);

	const [am, setAm] = useState(ammo);

	useEffect(() => {
		setAm(ammo);
	}, [ammo]);

	const returnColumns = (columns: any) => {
		let totalCells: number = 0;

		Object.keys(columns).forEach((k: string) => {
			totalCells += columns[k].subColumns
				? columns[k].subColumns.length
				: columns[k].name === 'Notes'
				? 3
				: 1;
		});

		return Object.keys(columns).map((k: string, idx: number, arr: any) => (
			<Column
				key={uuidv4()}
				data={columns[k]}
				len={arr.length}
				grids={totalCells}
				spanLen={
					columns[k]?.name === 'Notes' ||
					columns[k]?.name === 'Weapon' ||
					columns[k]?.name === 'Ammo'
						? 3
						: 1
				}
			/>
		));
	};

	const returnRows = (ammo: any) => {
		return Object.keys(ammo).map((k: string, idx: number, arr: any) => (
			<Row key={uuidv4()} data={ammo[k]} type={'ammo'} />
		));
	};

	return (
		<>
			<div id="ammoContainer" className={styles.ammoContainer}>
				<h3>Ammo</h3>

				<div className={styles.table}>
					{returnColumns(columns)}
					{returnRows(ammo)}
				</div>
			</div>
		</>
	);
}
