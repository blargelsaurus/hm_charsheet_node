import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface ammo {
	name: string;
	sRange: number;
	mRange: number;
	lRange: number;
	ammoName: string;
	weightEach: number;
	tinyDmg: string;
	smallDmg: string;
	medDmg: string;
	lgDmg: string;
	hugeDmg: string;
	giDmg: string;
	type: string;
	weight: number;
	notes: string;
}

export interface ammoState {
	columns: {
		[key: string]: column;
	};
	ammo: {
		[key: string]: ammo;
	};
}

const initialState: ammoState = {
	columns: {
		'0': {
			id: 0,
			name: 'Weapon',
			abbrev: 'Weapon',
		},
		'1': {
			id: 1,
			name: 'Range (Yards)',
			abbrev: 'Range (yds)',
			subColumns: ['S /', 'M /', 'L /'],
		},
		'2': {
			id: 2,
			name: 'Ammo',
			abbrev: 'Ammo',
		},
		'3': {
			id: 3,
			name: 'Weight Each',
			abbrev: 'Wgt./ea',
		},
		'4': {
			id: 4,
			name: 'Damage Versus',
			abbrev: 'Damage vs.',
			subColumns: ['T /', 'S /', 'M /', 'L /', 'H /', 'G /'],
		},
		'5': {
			id: 5,
			name: 'Damage Type',
			abbrev: 'Type',
		},
		'6': {
			id: 6,
			name: 'Weight (Pounds)',
			abbrev: 'Wgt. (Lbs.)',
		},
		'7': {
			id: 7,
			name: 'Notes',
			abbrev: 'Notes',
		},
	},
	ammo: {
		'0': {
			name: 'Dagger',
			sRange: 20,
			mRange: 40,
			lRange: 60,
			ammoName: 'Self',
			weightEach: 1,
			tinyDmg: '1d6+1',
			smallDmg: '1d6',
			medDmg: '1d6-1',
			lgDmg: '1d6-2',
			hugeDmg: '1d6-3',
			giDmg: '1d6-4',
			type: '1/1',
			weight: 0,
			notes: 'Something goes here...',
		},
	},
};

export const ammoSlice = createSlice({
	name: 'ammo',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = ammoSlice.actions;

export const getAmmo = (state: RootState) => state.ammo.ammo;
export const getAmmoCol = (state: RootState) => state.ammo.columns;

export default ammoSlice.reducer;
