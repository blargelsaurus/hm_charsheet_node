import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './general.module.css';

import { Column } from '../../column/Column';
import { v4 as uuidv4 } from 'uuid';
import { Row } from '../../rows/Row';
import { openModal } from '../../modal/modalSlice';

import {
	getGeneralCol,
	removeContainer,
	getItemList,
	items,
} from './generalSlice';

import containerTypes from '../../../items/containers.json';

interface gcProps {
	data: any;
	col?: any;
	type: string;
}

export function GeneralColumn({ data, col, type }: gcProps) {
	const dispatch = useDispatch();
	const items = useSelector(getItemList);

	const returnColumns = (columns: any) => {
		let totalCells: number = 0;

		Object.keys(columns).forEach((k: string) => {
			totalCells += columns[k].subColumns
				? columns[k].subColumns.length
				: columns[k].name === 'Notes'
				? 3
				: 1;
		});

		return Object.keys(columns).map((k: string, idx: number, arr: any) => (
			<Column
				key={uuidv4()}
				data={columns[k]}
				len={arr.length}
				grids={totalCells}
				spanLen={
					columns[k]?.name === 'Notes' ||
					columns[k]?.name === 'Weapon' ||
					columns[k]?.name === 'Ammo' ||
					columns[k]?.name === 'Armor' ||
					columns[k]?.name === 'Name'
						? 3
						: 1
				}
			/>
		));
	};

	const returnRows = (items: any, type?: string) => {
		switch (type) {
			case 'Talents':
			case 'Skills':
			case 'Racial':
			case 'Proficiency':
			case 'Quirks':
			case 'Languages':
				return Object.keys(items).map((k: string, idx: number, arr: any) => {
					return <Row key={uuidv4()} data={items[k]} type={'talents'} />;
				});

			default:
				return Object.keys(items).map((k: string, idx: number, arr: any) => {
					const updated = { ...items[k], btns: 'remove' };
					return <Row key={uuidv4()} data={updated} type={'items'} />;
				});
		}
	};

	const handleClick = (e: any, data?: any): void => {
		const btnLabel = e.target.dataset.label;
		const response: any = {};

		switch (btnLabel) {
			case 'addContainer':
				response['data'] = containerTypes;
				response['type'] = btnLabel;
				response['active'] = true;
				response['color'] = 'teal';

				dispatch(openModal(response));
				break;

			case 'removeContainer':
				dispatch(removeContainer(data.id));
				break;

			case 'addItem':
				response['type'] = btnLabel;
				response['active'] = true;
				response['color'] = 'teal';
				response['parentid'] = data.id;

				dispatch(openModal(response));
				break;

			default:
				break;
		}
	};

	const findItemsbyContainer = (items: any, guid: string): items[] => {
		return items.filter((it: items) => it.location === guid);
	};

	if (data) {
		switch (type) {
			case 'General Items':
				return (
					<div className={styles.tableData}>
						<div className={styles.tableTitle}>
							<p>
								<strong>{data.name}</strong>
							</p>
							<p>{data.weightMax}</p>
							<p>
								<a
									onClick={(e) => handleClick(e, data)}
									className={styles.removeBtn}
									data-label="removeContainer"
								>
									remove
								</a>
							</p>
						</div>
						<div className={styles.table}>
							{returnColumns(col)}
							{returnRows(findItemsbyContainer(items, data.id))}
						</div>
						<div className={styles.addItem}>
							<button
								data-label={'addItem'}
								onClick={(e) => handleClick(e, data)}
							>
								Add Item
							</button>
						</div>
					</div>
				);

			case 'Talents':
			case 'Racial':
			case 'Proficiency':
			case 'Quirks':
				return (
					<div className={styles.tableData}>
						<div className={styles.talentTable}>
							{returnColumns(col)}
							{returnRows(data, type)}
						</div>
					</div>
				);

			case 'Skills':
			case 'Languages':
				return (
					<div className={styles.tableData}>
						<div className={styles.table}>
							{returnColumns(col)}
							{returnRows(data, type)}
						</div>
					</div>
				);

			default:
				return <></>;
		}
	} else {
		return <></>;
	}
}
