import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { BasePrivateKeyEncodingOptions } from 'crypto';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
}

export interface items {
	itemId?: number | string;
	name: string;
	weight: number;
	location: string | number;
}

export interface bags {
	id: number | string;
	name: string;
	weightMax: number;
	action?: any;
}

export interface generalState {
	columns: {
		[key: string]: column;
	};
	itemList: items[];
	containers: bags[];
}

const initialState: generalState = {
	columns: {
		'0': {
			id: 0,
			name: 'Name',
			abbrev: 'Name',
		},
		'1': {
			id: 1,
			name: 'Weight',
			abbrev: 'Wgt.',
		},
		'2': {
			id: 2,
			name: '',
			abbrev: '',
		},
	},
	itemList: [],
	containers: [],
};

export const generalSlice = createSlice({
	name: 'general',
	initialState,
	reducers: {
		addContainer: (state, action: PayloadAction<bags>) => {
			state.containers.push({
				id: action.payload.id,
				name: action.payload.name,
				weightMax: action.payload.weightMax,
			});
		},

		removeContainer: (state, action: PayloadAction<string>) => {
			state.containers = state.containers.filter(
				(c) => c.id !== action.payload
			);
		},

		removeItem: (state, action: PayloadAction<string>) => {
			state.itemList = state.itemList.filter(
				(c) => c.itemId !== action.payload
			);
		},

		addItem: (state, action: PayloadAction<items>) => {
			let thisContainer = state.containers.find(
					(c) => c.id === action.payload.location
				),
				thisContainerItems = state.itemList.filter(
					(i) => i.location === action.payload.location
				),
				currentWeight = 0;

			thisContainerItems.forEach((el) => (currentWeight += el.weight));

			if (
				thisContainer &&
				action.payload.name &&
				action.payload.weight &&
				(currentWeight += action.payload.weight) <= thisContainer.weightMax
			) {
				state.itemList.push({
					location: action.payload.location,
					name: action.payload.name,
					weight: action.payload.weight,
					itemId: action.payload.itemId,
				});
			} else {
			}
		},
	},
});

export const { addContainer, addItem, removeContainer, removeItem } =
	generalSlice.actions;

export const getItemList = (state: RootState) => state.general.itemList;
export const getGeneralCol = (state: RootState) => state.general.columns;
export const getGeneralContainers = (state: RootState) =>
	state.general.containers;

export default generalSlice.reducer;

const getMax = (arr: number[]): number => {
	return arr.reduce(function (a, b) {
		return maxNumber(a, b);
	}, 0);
};

const maxNumber = (a: number, b: number): number => {
	return Math.max(a, b);
};
