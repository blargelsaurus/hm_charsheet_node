import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './general.module.css';

import { GeneralColumn } from './GeneralColumn';

import { getGeneralContainers, getGeneralCol } from './generalSlice';

import { openModal } from '../../modal/modalSlice';
import containerTypes from '../../../items/containers.json';

// interface Response {
// 	data?: any;
// 	type: string;
// 	active: boolean;
// 	color?: string;
// }

export interface btnProps {
	text: string;
	tag: string;
}

interface generalProps {
	label: string;
	columns: number;
	data?: any;
	btnInfo?: btnProps;
	colInfo?: any;
	loadAll: boolean;
}

export function General({
	label,
	columns,
	btnInfo,
	data,
	colInfo,
	loadAll,
}: generalProps) {
	const dispatch = useDispatch();
	const [dict, setDict] = useState<any>();
	const [cols, setCols] = useState<any>();

	const containers = useSelector(getGeneralContainers);
	const colData = useSelector(getGeneralCol);

	useEffect(() => {
		data ? setDict(data) : setDict(containers);
	}, [containers, data]);

	useEffect(() => {
		colInfo ? setCols(colInfo) : setCols(colData);
	}, [colData, colInfo]);

	const handleClick = (e: any, data?: any): void => {
		const btnLabel = e.target.dataset.label;
		const response: any = {};

		switch (btnLabel) {
			case 'addContainer':
				response['data'] = containerTypes;
				response['type'] = btnLabel;
				response['active'] = true;
				response['color'] = 'teal';

				dispatch(openModal(response));
				break;

			case 'addItem':
				response['type'] = btnLabel;
				response['active'] = true;
				response['color'] = 'teal';
				response['parentid'] = data.id;

				dispatch(openModal(response));
				break;

			default:
				break;
		}
	};

	const loadContainers = (data: any, col: number, loadAll: boolean) => {
		let response = [],
			i = 0;

		while (i < col) {
			response.push(
				<div
					data-label={data.id}
					className={col > 1 ? styles.tableContent : styles.tableContentFull}
				>
					{col > 1 ? (
						Object.keys(data)
							.filter((c: any, idx: number) => idx % col === i)
							.map((d: any) => {
								//console.log(data[d]);
								return <GeneralColumn data={data[d]} col={cols} type={label} />;
							})
					) : (
						<GeneralColumn data={data} col={cols} type={label} />
					)}
				</div>
			);
			i++;
		}

		return response;
	};

	return (
		<>
			<div id="generalContainer" className={styles.generalContainer}>
				<div className={styles.titleBar}>
					<h3>{label}</h3>

					{btnInfo && (
						<button data-label={btnInfo.tag} onClick={(e) => handleClick(e)}>
							{btnInfo.text}
						</button>
					)}
				</div>
				<div className={styles.containerContent}>
					{dict && loadContainers(dict, columns, loadAll)}
				</div>
			</div>
		</>
	);
}
