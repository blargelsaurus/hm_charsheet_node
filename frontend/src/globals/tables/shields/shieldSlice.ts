import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface shield {
	name: string;
	weight: number;
	ac: number;
	ac8: number | string;
	ac7: number | string;
	ac6: number | string;
	ac5: number | string;
	ac4: number | string;
	ac3: number | string;
	ac2: number | string;
	ac1: number | string;
	ac0: number | string;
}

export interface shieldState {
	columns: {
		[key: string]: column;
	};
	shield: {
		[key: string]: shield;
	};
}

const initialState: shieldState = {
	columns: {
		'0': {
			id: 0,
			name: 'Shield',
			abbrev: 'Shield',
		},
		'1': {
			id: 1,
			name: 'Weight',
			abbrev: 'Wgt.',
		},
		'2': {
			id: 2,
			name: 'AC',
			abbrev: 'AC',
		},
		'3': {
			id: 3,
			name: 'AC Points',
			abbrev: 'Pts.',
			subColumns: ['8', '7', '6', '5', '4', '3', '2', '1', '0'],
		},
	},
	shield: {
		'0': {
			name: 'Shield, Medium',
			weight: 10,
			ac: 3,
			ac8: '',
			ac7: '',
			ac6: '',
			ac5: '',
			ac4: '',
			ac3: 5,
			ac2: 4,
			ac1: 3,
			ac0: 0,
		},
	},
};

export const shieldSlice = createSlice({
	name: 'shield',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = shieldSlice.actions;

export const getShield = (state: RootState) => state.shield.shield;
export const getShieldCol = (state: RootState) => state.shield.columns;

export default shieldSlice.reducer;
