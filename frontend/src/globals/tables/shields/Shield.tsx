import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './shield.module.css';

import { column, getShield, getShieldCol } from './shieldSlice';
import { Column } from '../../column/Column';

import { v4 as uuidv4 } from 'uuid';
import { Row } from '../../rows/Row';

export function Shield() {
	const columns = useSelector(getShieldCol);
	const shield = useSelector(getShield);

	const [shld, setShld] = useState(shield);

	useEffect(() => {
		setShld(shield);
	}, [shield]);

	const returnColumns = (columns: any) => {
		let totalCells: number = 0;

		Object.keys(columns).forEach((k: string) => {
			totalCells += columns[k].subColumns
				? columns[k].subColumns.length
				: columns[k].name === 'Notes'
				? 3
				: 1;
		});

		return Object.keys(columns).map((k: string, idx: number, arr: any) => (
			<Column
				key={uuidv4()}
				data={columns[k]}
				len={arr.length}
				grids={totalCells}
				spanLen={
					columns[k]?.name === 'Notes' ||
					columns[k]?.name === 'Weapon' ||
					columns[k]?.name === 'Ammo' ||
					columns[k]?.name === 'Armor' ||
					columns[k]?.name === 'Shield'
						? 3
						: 1
				}
			/>
		));
	};

	const returnRows = (shield: any) => {
		return Object.keys(shield).map((k: string, idx: number, arr: any) => (
			<Row key={uuidv4()} data={shield[k]} type={'shield'} />
		));
	};

	return (
		<>
			<div id="shieldContainer" className={styles.shieldContainer}>
				<h3>Shields</h3>

				<div className={styles.table}>
					{returnColumns(columns)}
					{returnRows(shield)}
				</div>
			</div>
		</>
	);
}
