import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './talents.module.css';

import { column, getTalents, getTalentCol } from './talentSlice';
import { General } from '../general/General';

export function Talents() {
	const talents = useSelector(getTalents);
	const columns = useSelector(getTalentCol);
	// const [tal, setTal] = useState(talents);

	// useEffect(() => {
	// 	setTal(talents);
	// }, [talents]);

	return (
		<>
			<div id="talentContainer" className={styles.talentContainer}>
				<General
					label={'Talents'}
					columns={1}
					data={talents}
					colInfo={columns}
					loadAll={true}
				/>
			</div>
		</>
	);
}
