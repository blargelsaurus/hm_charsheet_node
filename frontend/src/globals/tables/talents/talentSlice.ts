import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface talent {
	itemId?: number | string;
	name: string;
}

export interface talentState {
	columns: {
		[key: string]: column;
	};
	talents: talent[];
}

const initialState: talentState = {
	columns: {
		'0': {
			id: 0,
			name: 'Name',
			abbrev: 'Name',
		},
	},
	talents: [
		{
			itemId: 1,
			name: 'Damage Cap Bonus - Short Sword',
		},
		{
			itemId: 2,
			name: 'Damage Cap Bonus - Short Sword',
		},
		{
			itemId: 3,
			name: 'Short Sword Bonus',
		},
	],
};

export const talentSlice = createSlice({
	name: 'talents',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = talentSlice.actions;

export const getTalents = (state: RootState) => state.talents.talents;
export const getTalentCol = (state: RootState) => state.talents.columns;

export default talentSlice.reducer;
