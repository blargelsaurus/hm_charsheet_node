import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface skill {
	itemId?: number | string;
	name: string;
	mastery: number;
}

export interface skillState {
	columns: {
		[key: string]: column;
	};
	skills: skill[];
	languages: skill[];
}

const initialState: skillState = {
	columns: {
		'0': {
			id: 0,
			name: 'Name',
			abbrev: 'Name',
		},
		'1': {
			id: 1,
			name: 'Mastery',
			abbrev: 'Mastery',
		},
		'2': {
			id: 2,
			name: '',
			abbrev: '',
		},
	},
	skills: [
		{
			itemId: 1,
			name: 'Military History - Myrmidon Package',
			mastery: 32,
		},
		{
			itemId: 2,
			name: 'Basic Leadership - Myrmidon Package',
			mastery: 10,
		},
		{
			itemId: 3,
			name: 'Campaign Logistics - Myrmidon Package',
			mastery: 19,
		},
		{
			itemId: 4,
			name: 'Military: Battle Sense - Myrmidon Package',
			mastery: 16,
		},
		{
			itemId: 5,
			name: 'Shield Punch',
			mastery: 48,
		},
		{
			itemId: 6,
			name: 'Trip Attack',
			mastery: 28,
		},
		{
			itemId: 7,
			name: 'Armor Repair, Basic',
			mastery: 11,
		},
		{
			itemId: 8,
			name: 'Jumping',
			mastery: 21,
		},
		{
			itemId: 9,
			name: 'Observation',
			mastery: 8,
		},
		{
			itemId: 10,
			name: 'Riding, Land-Based (porcine)',
			mastery: 17,
		},
		{
			itemId: 11,
			name: 'Seduction, Art of',
			mastery: 13,
		},
		{
			itemId: 12,
			name: 'Swimming: Dog Paddle',
			mastery: 22,
		},
		{
			itemId: 13,
			name: 'Tumbling',
			mastery: 20,
		},
	],
	languages: [
		{
			itemId: 1,
			name: 'Common',
			mastery: 100,
		},
		{
			itemId: 2,
			name: 'Reading/Writing - Common',
			mastery: 14,
		},
	],
};

export const skillSlice = createSlice({
	name: 'skills',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = skillSlice.actions;

export const getSkills = (state: RootState) => state.skills.skills;
export const getLanguages = (state: RootState) => state.skills.languages;
export const getCol = (state: RootState) => state.skills.columns;

export default skillSlice.reducer;
