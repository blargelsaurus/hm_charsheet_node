import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './skills.module.css';

import { getSkills, getCol, getLanguages } from './skillsSlice';
import { General } from '../general/General';

export function Skills() {
	const skills = useSelector(getSkills);
	const languages = useSelector(getLanguages);
	const columns = useSelector(getCol);

	return (
		<>
			<div id="skillContainer" className={styles.skillContainer}>
				<General
					label={'Skills'}
					columns={1}
					data={skills}
					colInfo={columns}
					loadAll={true}
				/>
				<General
					label={'Languages'}
					columns={1}
					data={languages}
					colInfo={columns}
					loadAll={true}
				/>
			</div>
		</>
	);
}
