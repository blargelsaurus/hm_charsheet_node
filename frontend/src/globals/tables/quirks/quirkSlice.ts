import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface column {
	id?: number;
	name: string;
	abbrev: string;
	subColumns?: string[];
}

export interface quirk {
	itemId?: number | string;
	name: string;
}

export interface quirkState {
	columns: {
		[key: string]: column;
	};
	quirks: quirk[];
}

const initialState: quirkState = {
	columns: {
		'0': {
			id: 0,
			name: 'Name',
			abbrev: 'Name',
		},
	},
	quirks: [
		{
			itemId: 1,
			name: '20% Tithes to Pangrus',
		},
		{
			itemId: 2,
			name: 'Nagging Concience',
		},
		{
			itemId: 3,
			name: 'Misguided',
		},
	],
};

export const quirkSlice = createSlice({
	name: 'quirk',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = quirkSlice.actions;

export const getQuirks = (state: RootState) => state.quirk.quirks;
export const getCol = (state: RootState) => state.quirk.columns;

export default quirkSlice.reducer;
