import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './quirk.module.css';

import { getQuirks, getCol } from './quirkSlice';
import { General } from '../general/General';

export function Quirks() {
	const quirks = useSelector(getQuirks);
	const columns = useSelector(getCol);

	return (
		<>
			<div id="quirkContainer" className={styles.quirkContainer}>
				<General
					label={'Quirks'}
					columns={1}
					data={quirks}
					colInfo={columns}
					loadAll={true}
				/>
			</div>
		</>
	);
}
