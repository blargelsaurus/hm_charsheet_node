import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './mainArea.module.css';

import { Summary } from '../stats/summary/Summary';
import { TabNav } from './tabNav/TabNav';
import { Inventory } from './tabContainer/inventory/Inventory';

export function MainArea() {
	return (
		<>
			<div id="mainContainer" className={styles.mainContainer}>
				<Summary />
				<div className={styles.mainContent}>
					<TabNav />
					<div>
						<Inventory />
					</div>
				</div>
			</div>
		</>
	);
}
