import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../app/store';

export interface mainState {}

const initialState: mainState = {
	active: true,
};

export const mainAreaSlice = createSlice({
	name: 'main',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = mainAreaSlice.actions;

export const selectActive = (state: RootState) => state.main;

export default mainAreaSlice.reducer;
