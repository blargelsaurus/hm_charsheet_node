import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface invState {}

const initialState: invState = {
	active: true,
};

export const inventorySlice = createSlice({
	name: 'inventory',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = inventorySlice.actions;

export const selectActive = (state: RootState) => state.main;

export default inventorySlice.reducer;
