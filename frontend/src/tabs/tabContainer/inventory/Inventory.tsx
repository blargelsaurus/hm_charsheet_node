import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Weapons } from '../../../globals/tables/weapons/Weapons';
import { Ammo } from '../../../globals/tables/ammo/Ammo';
import styles from './inventory.module.css';

import { selectActive } from '../../tabNav/tabnavSlice';
import { Armor } from '../../../globals/tables/armor/Armor';
import { Shield } from '../../../globals/tables/shields/Shield';
import { btnProps, General } from '../../../globals/tables/general/General';
import { Spells } from '../../../globals/tables/spells/Spells';
import { Talents } from '../../../globals/tables/talents/Talents';
import { Skills } from '../../../globals/tables/skills/Skills';
import { Racials } from '../../../globals/tables/racial/Racials';
import { Proficiency } from '../../../globals/tables/proficiency/Proficiency';
import { Quirks } from '../../../globals/tables/quirks/Quirks';

export function Inventory() {
	const activeTab = useSelector(selectActive);

	const getContent = (activeTab: string) => {
		switch (activeTab) {
			case 'inventory':
				const btnInfo: btnProps = {
					text: 'Add new container',
					tag: 'addContainer',
				};

				return (
					<>
						<Weapons />
						<Ammo />
						<Armor />
						<Shield />
						<General
							label={'General Items'}
							columns={3}
							btnInfo={btnInfo}
							loadAll={false}
						/>
					</>
				);

			case 'skills, talents & spells':
				return (
					<>
						<Spells />
						<div className={styles.skillContainer}>
							<div className={styles.skillColumn}>
								<Talents />
								<Racials />
								<Proficiency />
								<Quirks />
							</div>
							<div className={styles.skillColumn}>
								<Skills />
							</div>
						</div>
					</>
				);

			default:
				return;
		}
	};

	return (
		<>
			<div id="inventoryContainer" className={styles.inventoryContainer}>
				{getContent(activeTab)}
			</div>
		</>
	);
}
