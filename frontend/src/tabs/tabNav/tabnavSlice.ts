import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface mainState {
	tabs: string[];
	activeTab: string;
}

const initialState: mainState = {
	tabs: [
		'inventory',
		'skills, talents & spells',
		'personal info',
		'companions',
		'wounds',
	],
	activeTab: 'inventory',
};

export const tabnavSlice = createSlice({
	name: 'tabnav',
	initialState,
	reducers: {
		changeActive: (state, action: PayloadAction<string>) => {
			state.activeTab = action.payload;
		},
	},
});

export const { changeActive } = tabnavSlice.actions;

export const selectActive = (state: RootState) => state.tabnav.activeTab;
export const selectTabNav = (state: RootState) => state.tabnav.tabs;

export default tabnavSlice.reducer;
