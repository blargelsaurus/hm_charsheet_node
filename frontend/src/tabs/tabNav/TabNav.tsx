import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './tabnav.module.css';

import { v4 as uuidv4 } from 'uuid';
import { Tab } from '../../globals/tab/Tab';
import { selectActive, selectTabNav } from './tabnavSlice';

const getTabs = (tabs: string[], type: string) => {
	return tabs.map((t) => <Tab key={uuidv4()} name={t} type={'primary'} />);
};

export function TabNav() {
	const tabs = useSelector(selectTabNav);
	return (
		<>
			<div id="tabnavContainer" className={styles.tabnavContainer}>
				<div className={styles.tabnav}>{getTabs(tabs, 'primary')}</div>
			</div>
		</>
	);
}
