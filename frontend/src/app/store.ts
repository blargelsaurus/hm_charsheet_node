import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import bioReducer from '../stats/components/bio/bioSlice';
import highLvlReducer from '../stats/components/highLvl/highLvlSlice';
import basicsReducer from '../stats/components/basicStats/basicstatsSlice';
import honorReducer from '../stats/components/honor/honorSlice';
import fameReducer from '../stats/components/fame/fameSlice';
import hpReducer from '../stats/components/hpArmor/hpSlice';
import moveReducer from '../stats/components/movement/movementSlice';
import adjustReducer from '../stats/components/adjustments/adjustSlice';
import doorReducer from '../stats/components/doors/doorsSlice';
import actionBarReducer from '../panels/action/bar/actionbarSlice';
import itemReducer from '../panels/action/items/itemsSlice';
import mainReducer from '../tabs/mainAreaSlice';
import charbarReducer from '../stats/summary/characterBar/characterBarSlice';
import statModsSlice from '../stats/summary/statMods/statModsSlice';
import modalSlice from '../globals/modal/modalSlice';
import weightSlice from '../stats/summary/weight/weightSlice';
import treasureSummSlice from '../stats/summary/treasure/treasureSummSlice';
import tabnavSlice from '../tabs/tabNav/tabnavSlice';
import inventorySlice from '../tabs/tabContainer/inventory/inventorySlice';
import weaponsSlice from '../globals/tables/weapons/weaponsSlice';
import ammoSlice from '../globals/tables/ammo/ammoSlice';
import armorSlice from '../globals/tables/armor/armorSlice';
import shieldSlice from '../globals/tables/shields/shieldSlice';
import generalSlice from '../globals/tables/general/generalSlice';
import spellsSlice from '../globals/tables/spells/spellsSlice';
import talentSlice from '../globals/tables/talents/talentSlice';
import skillsSlice from '../globals/tables/skills/skillsSlice';
import racialSlice from '../globals/tables/racial/racialSlice';
import proficiencySlice from '../globals/tables/proficiency/proficiencySlice';
import quirkSlice from '../globals/tables/quirks/quirkSlice';

export const store = configureStore({
	reducer: {
		counter: counterReducer,
		bio: bioReducer,
		highLvl: highLvlReducer,
		basics: basicsReducer,
		honor: honorReducer,
		fame: fameReducer,
		hp: hpReducer,
		movement: moveReducer,
		adjust: adjustReducer,
		door: doorReducer,
		actionBar: actionBarReducer,
		items: itemReducer,
		main: mainReducer,
		charbar: charbarReducer,
		statMods: statModsSlice,
		modal: modalSlice,
		weight: weightSlice,
		treasureSumm: treasureSummSlice,
		tabnav: tabnavSlice,
		inventory: inventorySlice,
		weapons: weaponsSlice,
		ammo: ammoSlice,
		armor: armorSlice,
		shield: shieldSlice,
		general: generalSlice,
		spells: spellsSlice,
		talents: talentSlice,
		skills: skillsSlice,
		racials: racialSlice,
		proficiency: proficiencySlice,
		quirk: quirkSlice,
	},
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;
