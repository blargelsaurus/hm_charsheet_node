import { useSelector } from 'react-redux';
import './App.css';

import { Stats } from './stats/components';
import { ActionPanel } from './panels/action';
import { MainArea } from './tabs/MainArea';
import { Overlay } from './globals/overlay/Overlay';
import { Modal } from './globals/modal/Modal';

import { isModalActive } from './globals/modal/modalSlice';

function App() {
	const modalActive = useSelector(isModalActive);

	return (
		<>
			<div className="App">
				<Stats />
				<ActionPanel />
				<MainArea />
			</div>

			<Overlay active={modalActive} />
			<Modal active={modalActive} />
		</>
	);
}

export default App;
