import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './saves.module.css';

export function Saves() {
	return (
		<section data-label="savesInfo" className={styles.savesContainer}>
			<h5>Save Rolls</h5>
		</section>
	);
}
