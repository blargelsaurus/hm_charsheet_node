import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './highlvl.module.css';

import {
    selectClass,
    selectTitle,
    selectRace,
    selectAlignment
} from './highLvlSlice'

export function HighLvl(){
    const cls = useSelector(selectClass);
    const title = useSelector(selectTitle);
    const race = useSelector(selectRace);
    const align = useSelector(selectAlignment);

    return (
        <>
            <section data-label="highLevelInfo" className={styles.hlContainer}>
                <div data-label="highLevelClass" className={styles.hlStat}>
                    <span>Class</span>
                    <h6>{cls}<span>{title}</span></h6>
                </div>

                <div data-label="highLevelRace" className={styles.hlStat}>
                    <span>Race</span>
                    <h6>{race}</h6>
                </div>

                <div data-label="highLevelAlignment" className={styles.hlStat}>
                    <span>Alignment</span>
                    <h6>{align}</h6>
                </div>
            </section>
        </>
    )
}