import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface highLvlState {
	class: string[];
	title: string;
	race: string;
	alignment: string;
}

const initialState: highLvlState = {
	class: ['fighter'],
	title: 'veteran',
	race: 'Gnome Titan',
	alignment: 'Chaotic Neutral',
};

export const highLvlSlice = createSlice({
	name: 'highLvl',
	initialState,
	reducers: {
		setClass: (state, action: PayloadAction<string>) => {
			if (state.class.indexOf(action.payload) == -1)
				state.class.push(action.payload);
		},
		setTitle: (state, action: PayloadAction<string>) => {
			state.title = action.payload;
		},
		setRace: (state, action: PayloadAction<string>) => {
			state.race = action.payload;
		},
		setAlignment: (state, action: PayloadAction<string>) => {
			state.alignment = action.payload;
		},
	},
});

export const { setClass, setTitle, setRace, setAlignment } =
	highLvlSlice.actions;

export const selectClass = (state: RootState) => state.highLvl.class;
export const selectTitle = (state: RootState) => state.highLvl.title;
export const selectRace = (state: RootState) => state.highLvl.race;
export const selectAlignment = (state: RootState) => state.highLvl.alignment;

export default highLvlSlice.reducer;
