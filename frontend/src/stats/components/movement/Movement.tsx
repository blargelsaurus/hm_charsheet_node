import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './movement.module.css';

import { selectEncum, selectWalk, selectFly } from './movementSlice';

export function Movement() {
	const encumbrance = useSelector(selectEncum);
	const walk = useSelector(selectWalk);
	const fly = useSelector(selectFly);

	return (
		<>
			<section data-label="moveInfo" className={styles.moveContainer}>
				<h5>Movement</h5>
				<div className={styles.moveStatContainer}>
					<div data-label="moveEncumbrance" className={styles.moveStat}>
						<h6>{encumbrance}</h6>
						<span>Encumbrance</span>
					</div>
					<div className={styles.moveStatSubContainer}>
						<div data-label="moveWalk" className={styles.moveStat}>
							<h6>{walk}</h6>
							<span>Walk</span>
						</div>
						<div data-label="moveFly" className={styles.moveStat}>
							<h6>{fly}</h6>
							<span>Fly</span>
						</div>
					</div>
				</div>
			</section>
		</>
	);
}
