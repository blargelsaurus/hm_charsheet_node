import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface moveState {
	encumbrance: string;
	walk: number;
	fly: number;
}

const initialState: moveState = {
	encumbrance: 'none',
	walk: 6,
	fly: 18,
};

export const movementSlice = createSlice({
	name: 'movement',
	initialState,
	reducers: {
		setEncumbrance: (state, action: PayloadAction<string>) => {
			state.encumbrance = action.payload;
		},
		setWalk: (state, action: PayloadAction<number>) => {
			state.walk = action.payload;
		},
		setFly: (state, action: PayloadAction<number>) => {
			state.fly = action.payload;
		},
	},
});

export const { setEncumbrance, setWalk, setFly } = movementSlice.actions;

export const selectEncum = (state: RootState) => state.movement.encumbrance;
export const selectWalk = (state: RootState) => state.movement.walk;
export const selectFly = (state: RootState) => state.movement.fly;

export default movementSlice.reducer;
