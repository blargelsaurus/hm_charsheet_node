import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './honor.module.css';

import {
	selectBase,
	selectDie,
	selectChkMod,
	selectPresence,
} from './honorSlice';

export function Honor() {
	const base = useSelector(selectBase);
	const die = useSelector(selectDie);
	const chkMod = useSelector(selectChkMod);
	const pres = useSelector(selectPresence);

	return (
		<>
			<section data-label="honorInfo" className={styles.honorContainer}>
				<h5>Honor</h5>
				<div className={styles.honorStatContainer}>
					<div data-label="honorBase" className={styles.honorStat}>
						<h6>{base}</h6>
						<span>Base</span>
					</div>
					<div data-label="honorDie" className={styles.honorStat}>
						<h6>{die}</h6>
						<span>Die</span>
					</div>
					<div data-label="honorBase" className={styles.honorStat}>
						<h6>{chkMod}</h6>
						<span>Chk. Mod</span>
					</div>
					<div data-label="honorDie" className={styles.honorStat}>
						<h6>{pres}</h6>
						<span>Pres Factor</span>
					</div>
				</div>
			</section>
		</>
	);
}
