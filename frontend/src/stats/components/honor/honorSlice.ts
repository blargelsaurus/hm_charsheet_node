import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface honorState {
	base: number;
	die: string;
	chkMod: number;
	presence: string;
}

const initialState: honorState = {
	base: 24.25,
	die: 'd4',
	chkMod: 3,
	presence: '7 + Die',
};

export const honorSlice = createSlice({
	name: 'honor',
	initialState,
	reducers: {
		setBase: (state, action: PayloadAction<number>) => {
			state.base = action.payload;
		},
		setDie: (state, action: PayloadAction<string>) => {
			state.die = action.payload;
		},
		setChkMod: (state, action: PayloadAction<number>) => {
			state.chkMod = action.payload;
		},
		setPresence: (state, action: PayloadAction<string>) => {
			state.presence = action.payload;
		},
	},
});

export const { setBase, setDie, setChkMod, setPresence } = honorSlice.actions;

export const selectBase = (state: RootState) => state.honor.base;
export const selectDie = (state: RootState) => state.honor.die;
export const selectChkMod = (state: RootState) => state.honor.chkMod;
export const selectPresence = (state: RootState) => state.honor.presence;

export default honorSlice.reducer;
