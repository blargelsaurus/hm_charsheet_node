import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './adj.module.css';

import {
	selectHitProb,
	selectHp,
	selectDmg,
	selectMsl,
	selectReactn,
	selectReact,
	selectDef,
	selectMDef,
} from './adjustSlice';

export function Adjust() {
	const hitProb = useSelector(selectHitProb);
	const hp = useSelector(selectHp);
	const dmg = useSelector(selectDmg);
	const msl = useSelector(selectMsl);
	const reactn = useSelector(selectHitProb);
	const react = useSelector(selectHp);
	const def = useSelector(selectDmg);
	const mDef = useSelector(selectMsl);

	return (
		<>
			<section data-label="adjustInfo" className={styles.adjustContainer}>
				<h5>Adjustments</h5>
				<div className={styles.adjustStatContainer}>
					<div data-label="adjustHitProb" className={styles.adjustStat}>
						<h6>{hitProb}</h6>
						<span>Hit Prob.</span>
					</div>
					<div data-label="adjustHp" className={styles.adjustStat}>
						<h6>{hp}</h6>
						<span>Hp.</span>
					</div>
					<div data-label="adjustDmg" className={styles.adjustStat}>
						<h6>{dmg}</h6>
						<span>Dmg.</span>
					</div>
					<div data-label="adjustMsl" className={styles.adjustStat}>
						<h6>{msl}</h6>
						<span>Msl.</span>
					</div>
					<div data-label="adjustReactn" className={styles.adjustStat}>
						<h6>{reactn}</h6>
						<span>Reactn.</span>
					</div>
					<div data-label="adjustReact" className={styles.adjustStat}>
						<h6>{react}</h6>
						<span>React.</span>
					</div>
					<div data-label="adjustDef" className={styles.adjustStat}>
						<h6>{def}</h6>
						<span>Def.</span>
					</div>
					<div data-label="adjustMDef" className={styles.adjustStat}>
						<h6>{mDef}</h6>
						<span>Mag. Def.</span>
					</div>
				</div>
			</section>
		</>
	);
}
