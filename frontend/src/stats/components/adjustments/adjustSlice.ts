import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface adjustState {
	hitProb: number;
	hp: number;
	dmg: number;
	msl: number;
	reactn: number;
	react: number;
	def: number;
	mDef: number;
}

const initialState: adjustState = {
	hitProb: 3,
	hp: 4,
	dmg: 6,
	msl: 0,
	reactn: 0,
	react: -1,
	def: 0,
	mDef: 0,
};

export const adjustSlice = createSlice({
	name: 'adjust',
	initialState,
	reducers: {
		setHitProf: (state, action: PayloadAction<number>) => {
			state.hitProb = action.payload;
		},
		setHp: (state, action: PayloadAction<number>) => {
			state.hp = action.payload;
		},
		setDmg: (state, action: PayloadAction<number>) => {
			state.dmg = action.payload;
		},
		setMsl: (state, action: PayloadAction<number>) => {
			state.msl = action.payload;
		},
		setReactn: (state, action: PayloadAction<number>) => {
			state.reactn = action.payload;
		},
		setReact: (state, action: PayloadAction<number>) => {
			state.react = action.payload;
		},
		setDef: (state, action: PayloadAction<number>) => {
			state.def = action.payload;
		},
		setMDef: (state, action: PayloadAction<number>) => {
			state.mDef = action.payload;
		},
	},
});

export const {
	setHitProf,
	setHp,
	setDmg,
	setMsl,
	setReactn,
	setReact,
	setDef,
	setMDef,
} = adjustSlice.actions;

export const selectHitProb = (state: RootState) => state.adjust.hitProb;
export const selectHp = (state: RootState) => state.adjust.hp;
export const selectDmg = (state: RootState) => state.adjust.dmg;
export const selectMsl = (state: RootState) => state.adjust.msl;
export const selectReactn = (state: RootState) => state.adjust.reactn;
export const selectReact = (state: RootState) => state.adjust.react;
export const selectDef = (state: RootState) => state.adjust.def;
export const selectMDef = (state: RootState) => state.adjust.mDef;

export default adjustSlice.reducer;
