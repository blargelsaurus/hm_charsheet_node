import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './doors.module.css';

import { selectBBLG, selectLocked, selectStuck } from './doorsSlice';

export function Doors() {
	const bblg = useSelector(selectBBLG);
	const locked = useSelector(selectLocked);
	const stuck = useSelector(selectStuck);

	return (
		<>
			<section data-label="doorsInfo" className={styles.doorsContainer}>
				<div data-label="doorsArmor" className={styles.doorsStatSingle}>
					<h5>BB/LG</h5>
					<h6>{bblg}%</h6>
				</div>

				<div
					data-label="doorsStatContainer"
					className={styles.doorsStatContainer}
				>
					<h5>Open Doors Rolls</h5>
					<div data-label="doorsStuck" className={styles.doorsStat}>
						<h6>{stuck}</h6>
						<span>Stuck</span>
					</div>
					<div data-label="doorsLocked" className={styles.doorsStat}>
						<h6>{locked}</h6>
						<span>Locked</span>
					</div>
				</div>
			</section>
		</>
	);
}
