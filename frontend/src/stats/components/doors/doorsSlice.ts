import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface doorState {
	bblg: number;
	stuck: number;
	locked: number;
}

const initialState: doorState = {
	bblg: 35,
	stuck: 14,
	locked: 6,
};

export const doorSlice = createSlice({
	name: 'door',
	initialState,
	reducers: {
		setBBLG: (state, action: PayloadAction<number>) => {
			state.bblg = action.payload;
		},
		setStuck: (state, action: PayloadAction<number>) => {
			state.stuck = action.payload;
		},
		setLocked: (state, action: PayloadAction<number>) => {
			state.locked = action.payload;
		},
	},
});

export const { setBBLG, setStuck, setLocked } = doorSlice.actions;

export const selectBBLG = (state: RootState) => state.door.bblg;
export const selectStuck = (state: RootState) => state.door.stuck;
export const selectLocked = (state: RootState) => state.door.locked;

export default doorSlice.reducer;
