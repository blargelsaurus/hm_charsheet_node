import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './statStyles.module.css';

import { Bio } from './bio/Bio';
import { HighLvl } from './highLvl/HighLvl';
import { BasicStats } from './basicStats/BasicStats';
import { Honor } from './honor/Honor';
import { Fame } from './fame/Fame';
import { Hp } from './hpArmor/Hp';
import { Movement } from './movement/Movement';
import { Adjust } from './adjustments/Adjust';
import { Doors } from './doors/Doors';
import { Saves } from './saves/Saves';

export function Stats() {
	return (
		<>
			<div className={styles.statContainer}>
				<h3>Basic Stats</h3>

				<Bio />
				<HighLvl />
				<BasicStats />
				<Honor />
				<Fame />
				<Hp />
				<Movement />
				<Adjust />
				<Doors />
				<Saves />
			</div>
		</>
	);
}
