import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './basicStats.module.css';

import {
	selectStr,
	selectDex,
	selectCon,
	selectInt,
	selectWis,
	selectCha,
	selectCom,
} from './basicstatsSlice';

export function BasicStats() {
	const str = useSelector(selectStr);
	const dex = useSelector(selectDex);
	const con = useSelector(selectCon);
	const int = useSelector(selectInt);
	const wis = useSelector(selectWis);
	const cha = useSelector(selectCha);
	const com = useSelector(selectCom);

	return (
		<section data-label="basicInfo" className={styles.basicContainer}>
			<div data-label="basicStr" className={styles.basicStat}>
				<span>Str</span>
				<p>{str}</p>
			</div>

			<div data-label="basicDex" className={styles.basicStat}>
				<span>Dex</span>
				<p>{dex}</p>
			</div>

			<div data-label="basicCon" className={styles.basicStat}>
				<span>Con</span>
				<p>{con}</p>
			</div>

			<div data-label="basicInt" className={styles.basicStat}>
				<span>Int</span>
				<p>{int}</p>
			</div>

			<div data-label="basicWis" className={styles.basicStat}>
				<span>Wis</span>
				<p>{wis}</p>
			</div>

			<div data-label="basicCha" className={styles.basicStat}>
				<span>Cha</span>
				<p>{cha}</p>
			</div>

			<div data-label="basicCom" className={styles.basicStat}>
				<span>Com</span>
				<p>{com}</p>
			</div>
		</section>
	);
}
