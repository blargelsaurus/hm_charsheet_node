import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface basicState {
	strength: number;
	dexterity: number;
	constitution: number;
	intelligence: number;
	wisdom: number;
	charisma: number;
	commliness: number;
}

const initialState: basicState = {
	strength: 18.58,
	dexterity: 9.7,
	constitution: 18.7,
	intelligence: 9.26,
	wisdom: 8.53,
	charisma: 7.4,
	commliness: 13.49,
};

export const basicSlice = createSlice({
	name: 'basics',
	initialState,
	reducers: {
		setStr: (state, action: PayloadAction<number>) => {
			state.strength = action.payload;
		},
		incrementStr: (state, action: PayloadAction<number>) => {
			state.strength += action.payload;
		},
		setDex: (state, action: PayloadAction<number>) => {
			state.dexterity = action.payload;
		},
		incrementDex: (state, action: PayloadAction<number>) => {
			state.dexterity += action.payload;
		},
		setCon: (state, action: PayloadAction<number>) => {
			state.constitution = action.payload;
		},
		incrementCon: (state, action: PayloadAction<number>) => {
			state.constitution += action.payload;
		},
		setInt: (state, action: PayloadAction<number>) => {
			state.intelligence = action.payload;
		},
		incrementInt: (state, action: PayloadAction<number>) => {
			state.intelligence += action.payload;
		},
		setWis: (state, action: PayloadAction<number>) => {
			state.wisdom = action.payload;
		},
		incrementWis: (state, action: PayloadAction<number>) => {
			state.wisdom += action.payload;
		},
		setCha: (state, action: PayloadAction<number>) => {
			state.charisma = action.payload;
		},
		incrementCha: (state, action: PayloadAction<number>) => {
			state.charisma += action.payload;
		},
		setCom: (state, action: PayloadAction<number>) => {
			state.commliness = action.payload;
		},
		incrementCom: (state, action: PayloadAction<number>) => {
			state.commliness += action.payload;
		},
	},
});

export const {
	setStr,
	setDex,
	setCon,
	setInt,
	setWis,
	setCha,
	setCom,
	incrementStr,
	incrementCon,
	incrementDex,
	incrementInt,
	incrementWis,
	incrementCha,
	incrementCom,
} = basicSlice.actions;

export const selectStr = (state: RootState) => state.basics.strength;
export const selectDex = (state: RootState) => state.basics.dexterity;
export const selectCon = (state: RootState) => state.basics.constitution;
export const selectInt = (state: RootState) => state.basics.intelligence;
export const selectWis = (state: RootState) => state.basics.wisdom;
export const selectCha = (state: RootState) => state.basics.charisma;
export const selectCom = (state: RootState) => state.basics.commliness;

export default basicSlice.reducer;
