import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './fame.module.css';

import {
	selectFame,
	selectMaxHp,
	selectCurrentHp,
	selectTop,
} from './fameSlice';

export function Fame() {
	const fame = useSelector(selectFame);
	const max = useSelector(selectMaxHp);
	const curr = useSelector(selectCurrentHp);
	const top = useSelector(selectTop);

	return (
		<>
			<section data-label="fameInfo" className={styles.fameContainer}>
				<div data-label="fameFame" className={styles.fameStatSingle}>
					<h5>Fame</h5>
					<h6>{fame}</h6>
				</div>

				<div
					data-label="fameStatContainer"
					className={styles.fameStatContainer}
				>
					<h5>Hit Points</h5>
					<div data-label="fameMax" className={styles.fameStat}>
						<h6>{max}</h6>
						<span>Maximum</span>
					</div>
					<div data-label="fameCurrent" className={styles.fameStat}>
						<h6>{curr}</h6>
						<span>Current</span>
					</div>
					<div data-label="fameTop" className={styles.fameStat}>
						<h6>{top}</h6>
						<span>ToP</span>
					</div>
				</div>
			</section>
		</>
	);
}
