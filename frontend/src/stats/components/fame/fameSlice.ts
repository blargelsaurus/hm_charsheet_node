import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface fameState {
	fame: number;
	maxHp: number;
	currentHp: number;
	top: number;
}

const initialState: fameState = {
	fame: 24.25,
	maxHp: 33,
	currentHp: 33,
	top: 16.5,
};

export const fameSlice = createSlice({
	name: 'fame',
	initialState,
	reducers: {
		setFame: (state, action: PayloadAction<number>) => {
			state.fame = action.payload;
		},
		setMaxHp: (state, action: PayloadAction<number>) => {
			state.maxHp = action.payload;
		},
		setCurrentHp: (state, action: PayloadAction<number>) => {
			state.currentHp = action.payload;
		},
		setTop: (state, action: PayloadAction<number>) => {
			state.top = action.payload;
		},
		incrementHp: (state, action: PayloadAction<number>) => {
			state.currentHp += action.payload;
		},
		decreaseHp: (state, action: PayloadAction<number>) => {
			state.currentHp -= action.payload;
		},
	},
});

export const {
	setFame,
	setCurrentHp,
	setMaxHp,
	setTop,
	incrementHp,
	decreaseHp,
} = fameSlice.actions;

export const selectFame = (state: RootState) => state.fame.fame;
export const selectMaxHp = (state: RootState) => state.fame.maxHp;
export const selectCurrentHp = (state: RootState) => state.fame.currentHp;
export const selectTop = (state: RootState) => state.fame.top;

export default fameSlice.reducer;
