import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './hp.module.css';

import {
	selectShieldCurrPoints,
	selectShieldCurrStage,
	selectShieldPoints,
	selectShieldStage,
	selectBodyCurrPoints,
	selectBodyCurrStage,
	selectBodyPoints,
	selectBodyStage,
	selectHelmetCurrPoints,
	selectHelmetCurrStage,
	selectHelmetPoints,
	selectHelmetStage,
} from './hpSlice';

export function Hp() {
	const shieldCurrPoints = useSelector(selectShieldCurrPoints);
	const shieldCurrStage = useSelector(selectShieldCurrStage);
	const shieldPoints = useSelector(selectShieldPoints);
	const shieldStage = useSelector(selectShieldStage);

	const bodyCurrPoints = useSelector(selectBodyCurrPoints);
	const bodyCurrStage = useSelector(selectBodyCurrStage);
	const bodyPoints = useSelector(selectBodyPoints);
	const bodyStage = useSelector(selectBodyStage);

	const helmetCurrPoints = useSelector(selectHelmetCurrPoints);
	const helmetCurrStage = useSelector(selectHelmetCurrStage);
	const helmetPoints = useSelector(selectHelmetPoints);
	const helmetStage = useSelector(selectHelmetStage);

	return (
		<>
			<section data-label="hpInfo" className={styles.hpContainer}>
				<div data-label="hpStatContainer" className={styles.hpStatContainer}>
					<h5>Shield</h5>
					<div data-label="hpShieldPoints" className={styles.hpStat}>
						<h6>
							{shieldPoints}/{shieldCurrPoints}
						</h6>
						<span>Points Left</span>
					</div>
					<div data-label="hpShieldStage" className={styles.hpStat}>
						<h6>{shieldCurrStage}</h6>
						<span>AC</span>
					</div>
				</div>

				<div data-label="hpStatContainer" className={styles.hpStatContainer}>
					<h5>Helmet</h5>
					<div data-label="hpHelmetPoints" className={styles.hpStat}>
						<h6>
							{helmetPoints}/{helmetCurrPoints}
						</h6>
						<span>Points Left</span>
					</div>
					<div data-label="hpHelmentStage" className={styles.hpStat}>
						<h6>{helmetCurrStage}</h6>
						<span>AC</span>
					</div>
				</div>

				<div data-label="hpStatContainer" className={styles.hpStatContainer}>
					<h5>Body</h5>
					<div data-label="hpBodyPoints" className={styles.hpStat}>
						<h6>
							{bodyPoints}/{bodyCurrPoints}
						</h6>
						<span>Points Left</span>
					</div>
					<div data-label="hpBodyStage" className={styles.hpStat}>
						<h6>{bodyCurrStage}</h6>
						<span>AC</span>
					</div>
				</div>
			</section>
		</>
	);
}
