import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface hpState {
	shieldStages: number;
	shieldPoints: number;
	shieldCurrStage: number;
	shieldCurrPoints: number;

	helmetStages: number;
	helmetPoints: number;
	helmetCurrStage: number;
	helmetCurrPoints: number;

	bodyStages: number;
	bodyPoints: number;
	bodyCurrStage: number;
	bodyCurrPoints: number;
}

export interface actionAdjust {
	value: number;
	direction: string;
}

const initialState: hpState = {
	shieldStages: 3,
	shieldPoints: 3,
	shieldCurrStage: 3,
	shieldCurrPoints: 3,

	helmetStages: 2,
	helmetPoints: 3,
	helmetCurrStage: 2,
	helmetCurrPoints: 3,

	bodyStages: 3,
	bodyPoints: 12,
	bodyCurrStage: 3,
	bodyCurrPoints: 12,
};

export const hpSlice = createSlice({
	name: 'hp',
	initialState,
	reducers: {
		setShieldStage: (state, action: PayloadAction<number>) => {
			state.shieldStages = action.payload;
		},
		setHelmetStage: (state, action: PayloadAction<number>) => {
			state.helmetStages = action.payload;
		},
		setBodyStage: (state, action: PayloadAction<number>) => {
			state.bodyStages = action.payload;
		},

		setShieldPoints: (state, action: PayloadAction<number>) => {
			state.shieldPoints = action.payload;
		},
		setHelmetPoints: (state, action: PayloadAction<number>) => {
			state.helmetPoints = action.payload;
		},
		setBodyPoints: (state, action: PayloadAction<number>) => {
			state.bodyPoints = action.payload;
		},

		setShieldCurrStage: (state, action: PayloadAction<number>) => {
			state.shieldCurrStage = action.payload;
		},
		setHelmetCurrStage: (state, action: PayloadAction<number>) => {
			state.helmetCurrStage = action.payload;
		},
		setBodyCurrStage: (state, action: PayloadAction<number>) => {
			state.bodyCurrStage = action.payload;
		},

		setShieldCurrPoints: (state, action: PayloadAction<number>) => {
			state.shieldCurrPoints = action.payload;
		},
		adjustShieldCurrPoints: (state, action: PayloadAction<actionAdjust>) => {
			action.payload.direction == 'up'
				? (state.shieldCurrPoints += action.payload.value)
				: (state.shieldCurrPoints -= action.payload.value);
		},

		setHelmetCurrPoints: (state, action: PayloadAction<number>) => {
			state.helmetCurrPoints = action.payload;
		},
		adjustHelmetCurrPoints: (state, action: PayloadAction<actionAdjust>) => {
			action.payload.direction == 'up'
				? (state.helmetCurrPoints += action.payload.value)
				: (state.helmetCurrPoints -= action.payload.value);
		},

		setBodyCurrPoints: (state, action: PayloadAction<number>) => {
			state.bodyCurrPoints = action.payload;
		},
		adjustBodyCurrPoints: (state, action: PayloadAction<actionAdjust>) => {
			action.payload.direction == 'up'
				? (state.bodyCurrPoints += action.payload.value)
				: (state.bodyCurrPoints -= action.payload.value);
		},
	},
});

export const {} = hpSlice.actions;

export const selectShieldStage = (state: RootState) => state.hp.shieldStages;
export const selectShieldPoints = (state: RootState) => state.hp.shieldPoints;
export const selectShieldCurrStage = (state: RootState) =>
	state.hp.shieldCurrStage;
export const selectShieldCurrPoints = (state: RootState) =>
	state.hp.shieldCurrPoints;

export const selectHelmetStage = (state: RootState) => state.hp.helmetStages;
export const selectHelmetPoints = (state: RootState) => state.hp.helmetPoints;
export const selectHelmetCurrStage = (state: RootState) =>
	state.hp.helmetCurrStage;
export const selectHelmetCurrPoints = (state: RootState) =>
	state.hp.helmetCurrPoints;

export const selectBodyStage = (state: RootState) => state.hp.bodyStages;
export const selectBodyPoints = (state: RootState) => state.hp.bodyPoints;
export const selectBodyCurrStage = (state: RootState) => state.hp.bodyCurrStage;
export const selectBodyCurrPoints = (state: RootState) =>
	state.hp.bodyCurrPoints;

export default hpSlice.reducer;
