import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface bioState {
	age: number;
	weight: number;
	height: number;
	sex: string;
	handed: string;
}

const initialState: bioState = {
	age: 66,
	weight: 82,
	height: 43,
	sex: 'male',
	handed: 'ambidextrous',
};

export const bioSlice = createSlice({
	name: 'bio',
	initialState,
	reducers: {
		setAge: (state, action: PayloadAction<number>) => {
			state.age = action.payload;
		},
		setWeight: (state, action: PayloadAction<number>) => {
			state.weight = action.payload;
		},
		setHeight: (state, action: PayloadAction<number>) => {
			state.height = action.payload;
		},
		setSex: (state, action: PayloadAction<string>) => {
			state.sex = action.payload;
		},
		setHanded: (state, action: PayloadAction<string>) => {
			state.handed = action.payload;
		},
	},
});

export const { setAge, setWeight, setHeight, setSex, setHanded } =
	bioSlice.actions;

export const selectAge = (state: RootState) => state.bio.age;
export const selectWeight = (state: RootState) => state.bio.weight;
export const selectHeight = (state: RootState) => state.bio.height;
export const selectSex = (state: RootState) => state.bio.sex;
export const selectHanded = (state: RootState) => state.bio.handed;

export default bioSlice.reducer;
