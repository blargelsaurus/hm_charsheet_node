import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './bio.module.css'

import {
    selectAge,
    selectWeight,
    selectHeight,
    selectSex,
    selectHanded
} from './bioSlice'

export function Bio(){
    const age = useSelector(selectAge);
    const weight = useSelector(selectWeight);
    const height = useSelector(selectHeight);
    const sex = useSelector(selectSex);
    const handed = useSelector(selectHanded);

    return (
        <section data-label="bioInfo" className={styles.bioContainer}>
            <div data-label="bioAge" className={styles.bioStat}>
                <p>{age}</p>
                <span>Age</span>
            </div>
            <div data-label="bioWeight" className={styles.bioStat}>
                <p>{weight}</p>
                <span>Wt.</span>
            </div>
            <div data-label="bioHeight" className={styles.bioStat}>
                <p>{height}</p>
                <span>Ht.</span>
            </div>
            <div data-label="bioSex" className={styles.bioStat}>
                <p>{sex}</p>
                <span>Sex</span>
            </div>
            <div data-label="bioHanded" className={styles.bioStat}>
                <p>{handed}</p>
                <span>Handedness</span>
            </div>
        </section>
    );
}