import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface summState {}

const initialState: summState = {
	active: true,
};

export const summarySlice = createSlice({
	name: 'summary',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = summarySlice.actions;

export const selectActive = (state: RootState) => state.main;

export default summarySlice.reducer;
