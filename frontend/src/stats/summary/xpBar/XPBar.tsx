import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './xpBar.module.css';

export interface XPBarProps {
	currentXp: number;
	xpCap: number;
}

export function XPBar({ currentXp, xpCap }: XPBarProps) {
	const xp = currentXp < xpCap ? (currentXp / xpCap) * 100 : 100;
	const overflow =
		currentXp > xpCap ? ((currentXp - xpCap) / (xpCap + 1500)) * 100 : 0;

	const currentStyle = {
		width: `${xp}%`,
	};

	const overflowStyle = {
		width: `${overflow}%`,
	};

	return (
		<>
			<div id="xpContainer" className={styles.xpContainer}>
				<p>
					{currentXp} / {xpCap} XP
				</p>
				<div className={styles.barContainer}>
					<span style={currentStyle} className={styles.currentxp}></span>
					<span style={overflowStyle} className={styles.overflowxp}></span>
				</div>
			</div>
		</>
	);
}
