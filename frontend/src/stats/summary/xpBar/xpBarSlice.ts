import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface xpState {}

const initialState: xpState = {
	active: true,
};

export const xpBarSlice = createSlice({
	name: 'xp',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = xpBarSlice.actions;

export const selectActive = (state: RootState) => state.main;

export default xpBarSlice.reducer;
