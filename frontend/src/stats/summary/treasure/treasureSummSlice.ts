import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface tSummState {
	pp: number;
	gp: number;
	sp: number;
	cp: number;
}

const initialState: tSummState = {
	pp: 0,
	gp: 86,
	sp: 3,
	cp: 93,
};

export const treasureSummSlice = createSlice({
	name: 'treasureSumm',
	initialState,
	reducers: {
		setPP: (state, action: PayloadAction<number>) => {
			state.pp = action.payload;
		},
		setGP: (state, action: PayloadAction<number>) => {
			state.gp = action.payload;
		},
		setSP: (state, action: PayloadAction<number>) => {
			state.sp = action.payload;
		},
		setCP: (state, action: PayloadAction<number>) => {
			state.cp = action.payload;
		},
	},
});

export const { setPP, setGP, setSP, setCP } = treasureSummSlice.actions;

export const selectPP = (state: RootState) => state.treasureSumm.pp;
export const selectGP = (state: RootState) => state.treasureSumm.gp;
export const selectSP = (state: RootState) => state.treasureSumm.sp;
export const selectCP = (state: RootState) => state.treasureSumm.cp;

export default treasureSummSlice.reducer;
