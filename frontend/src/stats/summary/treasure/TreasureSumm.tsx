import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './treasureSumm.module.css';

import { selectPP, selectGP, selectSP, selectCP } from './treasureSummSlice';

export function TreasureSumm() {
	const pp = useSelector(selectPP);
	const gp = useSelector(selectGP);
	const sp = useSelector(selectSP);
	const cp = useSelector(selectCP);

	return (
		<>
			<section
				data-label="treasureSummInfo"
				className={styles.treasureSummContainer}
			>
				<p>Currency</p>
				<div className={styles.treasureSummContent}>
					<div data-label="pp" className={styles.tSummStat}>
						<h6>{pp}</h6>
						<span>PP</span>
					</div>
					<div data-label="gp" className={styles.tSummStat}>
						<h6>{gp}</h6>
						<span>GP</span>
					</div>
					<div data-label="sp" className={styles.tSummStat}>
						<h6>{sp}</h6>
						<span>SP</span>
					</div>
					<div data-label="cp" className={styles.tSummStat}>
						<h6>{cp}</h6>
						<span>CP</span>
					</div>
				</div>
			</section>
		</>
	);
}
