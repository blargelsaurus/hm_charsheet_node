import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface charBarState {
	lvl: number;
	name: string;
	xp: number;
	xpCap: number;
}

const initialState: charBarState = {
	lvl: 3,
	name: 'Ornn',
	xp: 1500,
	xpCap: 6000,
};

export const characterBarSlice = createSlice({
	name: 'charbar',
	initialState,
	reducers: {
		setName: (state, action: PayloadAction<string>) => {
			state.name = action.payload;
		},
		setLvl: (state, action: PayloadAction<number>) => {
			state.lvl = action.payload;
		},

		setXp: (state, action: PayloadAction<number>) => {
			state.xp = action.payload;
		},
		setXpCap: (state, action: PayloadAction<number>) => {
			state.xpCap = action.payload;
		},
	},
});

export const { setName, setLvl } = characterBarSlice.actions;

export const selectLvl = (state: RootState) => state.charbar.lvl;
export const selectName = (state: RootState) => state.charbar.name;
export const selectXp = (state: RootState) => state.charbar.xp;
export const selectXpCap = (state: RootState) => state.charbar.xpCap;

export default characterBarSlice.reducer;
