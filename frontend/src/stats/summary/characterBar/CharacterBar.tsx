import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './characterBar.module.css';

import { XPBar } from '../xpBar/XPBar';

import {
	selectLvl,
	selectName,
	selectXp,
	selectXpCap,
} from './characterBarSlice';

export function CharacterBar() {
	const name = useSelector(selectName);
	const lvl = useSelector(selectLvl);
	const xp = useSelector(selectXp);
	const cap = useSelector(selectXpCap);

	return (
		<>
			<div id="characterBarContainer" className={styles.characterBarContainer}>
				<h3>{lvl}</h3>
				<h3>{name}</h3>

				<XPBar currentXp={xp} xpCap={cap} />
			</div>
		</>
	);
}
