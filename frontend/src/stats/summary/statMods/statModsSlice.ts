import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface mod {
	id: number;
	name: string;
	desc: string;
	stat: string;
	change: number;
	increase: boolean;
}

export interface statModState {
	mods: {
		[key: string]: mod;
	};
}

const initialState: statModState = {
	mods: {
		'0': {
			id: 0,
			name: 'Charismatic Link',
			desc: 'The result of Charisma state change.',
			stat: 'COM',
			change: 1,
			increase: true,
		},
		'1': {
			id: 1,
			name: 'Strength Link',
			desc: 'Because I said so...',
			stat: 'STR',
			change: 3,
			increase: true,
		},
		'2': {
			id: 2,
			name: 'Drunk',
			desc: 'Struggle with Alcoholism',
			stat: 'WIS',
			change: 1,
			increase: false,
		},
	},
};

export const statModSlice = createSlice({
	name: 'statMods',
	initialState,
	reducers: {
		addMod: (state, action: PayloadAction<mod>) => {
			let keys: number[] = Object.keys(state.mods).map((i) => parseInt(i)),
				lastKey: number = getMax(keys),
				newKey: number = lastKey + 1;

			state.mods[newKey] = {
				id: newKey,
				name: action.payload.name,
				desc: action.payload.desc,
				stat: action.payload.stat,
				change: action.payload.change,
				increase: action.payload.increase,
			};
		},

		removeMod: (state, action: PayloadAction<string>) => {
			let keys: number[] = Object.keys(state.mods).map((i) => parseInt(i));

			keys.forEach((k) => {
				if (state.mods[k].name == action.payload) delete state.mods[k];
			});
		},
	},
});

export const { addMod, removeMod } = statModSlice.actions;

export const selectMods = (state: RootState) => state.statMods.mods;

export default statModSlice.reducer;

const getMax = (arr: number[]): number => {
	return arr.reduce(function (a, b) {
		return maxNumber(a, b);
	}, 0);
};

const maxNumber = (a: number, b: number): number => {
	return Math.max(a, b);
};
