import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './statMods.module.css';
import '../../../App.css';

import { selectMods } from './statModsSlice';
import { openModal } from '../../../globals/modal/modalSlice';
import { Tooltip } from '../../../globals/tooltip/Tooltip';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';

export function StatMods() {
	const dispatch = useDispatch();
	const mods = useSelector(selectMods);
	const modKeys = Object.keys(mods);

	const [hasTooltip, setHasTooltip] = useState(false);

	const getText = (keys: string[], arr: any): string => {
		let response = '';
		keys.forEach(
			(k: any, i: number, a: any) =>
				(response += `${arr[k]?.name}${a.length - 1 === i ? '' : ', '}` || '')
		);
		return response;
	};

	const handleClick = (e: any) => {
		let response = {
			data: mods,
			type: 'statmods',
			active: true,
			color: 'red',
		};
		dispatch(openModal(response));
	};

	const displayText = getText(modKeys, mods);

	useEffect(() => {
		const modCont = document.getElementById('statModsContainer'),
			txt = modCont?.querySelector('h6');

		txt &&
			modCont &&
			txt?.offsetWidth > modCont?.offsetWidth &&
			setHasTooltip(true);
	}, []);

	return (
		<>
			<div
				id="statModsContainer"
				className={styles.statModsContainer}
				onClick={(e) => handleClick(e)}
			>
				<div
					className={`${styles.statModItem} ${
						hasTooltip === true ? 'hasTooltip' : ''
					}`}
				>
					<span>
						Bonuses/Penalties <FontAwesomeIcon icon={faEllipsisH} />
					</span>
					<h6>{displayText}</h6>
					<Tooltip text={displayText} />
				</div>
			</div>
		</>
	);
}
