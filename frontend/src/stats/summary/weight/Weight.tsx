import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './weight.module.css';

import { selectWeight } from './weightSlice';

export function Weight() {
	const weight = useSelector(selectWeight);

	return (
		<>
			<section data-label="weightInfo" className={styles.weightContainer}>
				<div data-label="weight" className={styles.weightStat}>
					<span>Weight</span>
					<h6>{weight} lbs</h6>
				</div>
			</section>
		</>
	);
}
