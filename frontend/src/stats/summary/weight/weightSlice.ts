import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface weightState {
	weight: number;
}

const initialState: weightState = {
	weight: 75.23,
};

export const weightSlice = createSlice({
	name: 'weight',
	initialState,
	reducers: {
		setWeight: (state, action: PayloadAction<number>) => {
			state.weight = action.payload;
		},
	},
});

export const { setWeight } = weightSlice.actions;

export const selectWeight = (state: RootState) => state.weight.weight;

export default weightSlice.reducer;
