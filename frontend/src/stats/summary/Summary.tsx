import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './summary.module.css';

import { CharacterBar } from './characterBar/CharacterBar';
import { SummaryStat } from './summaryStats/SummaryStats';

export function Summary() {
	return (
		<>
			<div id="summaryContainer" className={styles.summaryContainer}>
				<CharacterBar />
				<SummaryStat />
			</div>
		</>
	);
}
