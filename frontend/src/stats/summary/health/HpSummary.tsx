import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './hpSummary.module.css';

import { selectCurrentHp } from '../../components/fame/fameSlice';

export function HpSummary() {
	const curr = useSelector(selectCurrentHp);

	return (
		<>
			<section data-label="hpSummInfo" className={styles.hpSummContainer}>
				<div data-label="hpSummCurrent" className={styles.hpSummStat}>
					<span>Current Hit Points</span>
					<h6>{curr}</h6>
				</div>
			</section>
		</>
	);
}
