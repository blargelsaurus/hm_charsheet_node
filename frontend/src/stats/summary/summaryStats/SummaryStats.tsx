import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './summaryStats.module.css';

import { StatMods } from '../statMods/StatMods';
import { HpSummary } from '../health/HpSummary';
import { Weight } from '../weight/Weight';
import { TreasureSumm } from '../treasure/TreasureSumm';

export function SummaryStat() {
	return (
		<>
			<div id="summaryStatContainer" className={styles.summaryStatContainer}>
				<HpSummary />
				<StatMods />
				<Weight />
				<TreasureSumm />
			</div>
		</>
	);
}
