import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface summStatState {}

const initialState: summStatState = {
	active: true,
};

export const summaryStatSlice = createSlice({
	name: 'summaryStat',
	initialState,
	reducers: {
		changeActive: (state) => {},
	},
});

export const { changeActive } = summaryStatSlice.actions;

export const selectActive = (state: RootState) => state.main;

export default summaryStatSlice.reducer;
