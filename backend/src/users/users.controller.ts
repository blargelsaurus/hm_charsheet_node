import { Controller, Get, Post } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  async getAllUsers(): Promise<string[]> {
    return this.userService.getAllUsers();
  }

  @Post(':id')
  async getUserByID(): Promise<string> {
    return this.userService.getUserByID('');
  }
}
