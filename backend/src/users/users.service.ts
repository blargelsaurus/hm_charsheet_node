import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {
  async getAllUsers(): Promise<string[]> {
    return ['Sean', 'Dani', 'Sharon'];
  }

  async getUserByID(id: string): Promise<string> {
    return 'test user';
  }
}
