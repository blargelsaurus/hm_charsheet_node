import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getHiChris(): string {
    return 'Hey Chris!';
  }
}
